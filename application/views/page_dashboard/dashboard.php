<!-- END: Subheader -->
<div class="m-content">
    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30"
        role="alert">
        <div class="m-alert__icon">
            <i class="flaticon-questions-circular-button m--font-brand"></i>
        </div>
        <div class="m-alert__text">
            Halaman ini digunakan untuk mengintegrasikan sistem <code>WA-Notify</code> dengan
            <code>Dashboard API Whatsapp</code>.
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        <?php echo $title ?>
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <div style="font-size:16px;">
                <div>
                    Sebelum menggunakan aplikasi ini, pastikan handphone dengan whatsapp admin dalam keadaan online. Apabila ingin mengulangi scan silahkan ikuti perintah dibawah ini.
                </div>
                <div>
                    <ol>
                        <li>Koneksikan terlebih dahulu dengan API Whatsapp
                            <a href="<?php echo $this->dataload->option('provider')->label ?>" target="_blank"
                                class="btn btn-success btn-sm mb-1">Klik di sini</a>
                        </li>
                        <li>Pastikan nomor Whatsapp terdaftar
                            <code>
                                    <?php echo $this->dataload->option('telp')->label ?>
                                </code>
                            masukkan nomor ini untuk mendapatkan kode OTP.
                        </li>
                        <li>Setelah masuk dashboard, cek status QR, jika <code>Offline</code> maka klik
                            <code>Generate OR</code>.
                        </li>
                        <li>Scan QR Code menggunakan handphone whatsapp admin.</li>
                        <li>Selesai, silahkan menggunakan aplikasi ini.</li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>