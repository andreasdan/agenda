<?php
// Form input text
function input_text($label,$type,$id,$name,$value,$placeholder,$attribute,$caption){
	echo '
		<div class="form-group row">
			<label for="name" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">'.$label.'</label>
			<div class="col-lg-8 col-md-9 col-sm-8">
				<input class="form-control" type="'.$type.'" id="'.$id.'" name="'.$name.'" value="'.$value.'" placeholder="'.$placeholder.'" autofocus '.$attribute.'>
				<small class="form-text text-muted">'.$caption.'</small>
			</div>
		</div>
	';
}

// Form input radio button
function input_radio($label,$array,$text,$name,$value,$attribute,$caption){
	$option="";
	$looping=0;
	foreach($array as $row){
		if($row==$value){
			$checked="checked";
		}else{
			$checked="";
		}
		$option=$option.'
						<div class="custom-control custom-radio">
							<input type="radio" id="'.$name.$row.'" name="'.$name.'" value="'.$row.'" '.$checked.' '.$attribute.' autofocus class="custom-control-input">
							<label class="custom-control-label" for="'.$name.$row.'">'.$text[$looping].'</label>
						</div>
						';
		$looping++;
	};

	echo '
		<div class="form-group row">
			<label class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">'.$label.'</label>
			<div class="col-lg-8 col-md-9 col-sm-8 d-flex align-items-center">
				'.$option.'
				<small class="form-text text-muted">'.$caption.'</small>
			</div>
		</div>
	';
}

// Form input option
function input_option($label,$id,$default,$array,$valueoption,$name,$value,$attribute,$caption){
	$option="";
	$looping=0;
	foreach($array as $row){
		if($row==$value){
			$selected="selected";
		}else{
			$selected="";
		}
		$option=$option."
						<option value='".$row."' ".$selected.">".$valueoption[$looping]."</option>
						";
		$looping++;
	};
	echo '
		<div class="form-group row">
			<label class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">'.$label.'</label>
			<div class="col-lg-2 col-md-9 col-sm-8 d-flex align-items-center">
				<div class="select2-input">
					<select id="'.$id.'" name="'.$name.'" '.$attribute.' class="form-control" autofocus>
						<option value="">'.$default.' pilih -</option>
						'.$option.'
						<small class="form-text text-muted">'.$caption.'</small>
					</select>
				</div>
			</div>
		</div>					
	';
}

// Form input date
function input_date($label,$type,$id,$name,$value,$attribute,$caption){
	echo '
		<div class="form-group form-show-validation row">
			<label for="birth" class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">'.$label.'</label>
			<div class="col-lg-6 col-md-9 col-sm-8">
				<div class="input-group">
					<input class="form-control" type="'.$type.'" id="'.$id.'" name="'.$name.'" value="'.$value.'" autofocus '.$attribute.'>
					<div class="input-group-append">
						<span class="input-group-text">
							<i class="fa fa-calendar-o"></i>
						</span>
					</div>
					<small class="form-text text-muted">'.$caption.'</small>
				</div>
			</div>
		</div>
	';
}

// Form input upload
function input_upload($label,$name,$attribute,$caption){
	echo '
		<div class="form-group row">
			<label class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right">'.$label.'</label>
			<div class="col-lg-4 col-md-9 col-sm-8">
				<div class="input-file input-file-image">
					<img class="img-upload-preview img-circle" width="100" height="100" src="'.base_url('assets/img/no-image.png').'" alt="preview">
					<input type="file" class="form-control form-control-file" id="uploadImg" name="'.$name.'" accept="image/*" '.$attribute.' >
					<label for="uploadImg" class="btn btn-primary btn-round btn-lg"><i class="fa fa-file-image"></i> Upload a Image</label>
					<small class="form-text text-muted">'.$caption.'</small>
				</div>
			</div>
		</div>
	';
}

//  form button save + cancel
function input_button(){
	echo '
		<div class="row">
			<label class="col-lg-3 col-md-3 col-sm-4 mt-sm-2 text-right"></label>
			<div class="col-lg-8 col-md-9 col-sm-8 d-flex align-items-center">
				<div class="custom-control custom-checkbox">
					<button type="submit" class="btn btn-success">Simpan</button>
					<button type="reset" onclick="goBack()" class="btn btn-danger">Batal</button>
					<script>
						function goBack() {
						  window.history.back();
						}
					</script>
				</div>
			</div>
		</div>
	';
}

function button_edit(){
	echo '
		<button data-toggle="modal" data-target="#edit" class="btn btn-info"><i class="icon-note"></i></button>
	';
}

function button_ubah($x){
	echo '
		<a href="'.$x.'" class="btn btn-info"><i class="icon-note"></i></a>
	';
}

function button_delete($x){
	echo '
		<a class="btn btn-danger" href="'.$x.'" onClick="return confirm('."'Apakah Anda yakin ingin menghapus data ini?'".')")><i class="icon-close"></i></a>
	';
}

function button_action($icon,$caption){
	echo '
		<button type="submit" class="btn btn-success">'.$icon.' '."$caption".'</button>
	';
}

function button_add($x,$y){
	echo '
		<a class="btn btn-success" '.$y.'><span class="btn-label"><i class="fa fa-plus"></i></span> Input '.$x.'</a>
	';
}

function button_approve($x,$y){
	echo '
		<button class="btn btn-success" '.$y.'><span class="btn-label"><i class="fas fa-check"></i></span> '.$x.'</button>
	';
}

function button_print($x,$y){
	echo '
		<button class="btn btn-dark" '.$y.'><span class="btn-label"><i class="fas fa-print"></i></span> '.$x.'</button>
	';
}

function button_ok($x,$y){
	echo '
		<button type="submit" class="btn btn-success" '.$y.'><span class="btn-label"><i class="fas fa-check"></i></span> '.$x.'</button>
	';
}
