<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Report extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('level') !== '1') {
            reject();
        }
        $this->load->library('encryption');
        $this->load->helper(array('form', 'url'));
    }

    public function index()
    {
        echo 'Not Found';
    }

    public function today()
    {
        $data['title'] = "Laporan Pengiriman Hari Ini";
        $data['url'] = base_url('report/show/today');
		$this->template->load('layout_dashboard', 'page_dashboard/report', $data);
    }

    public function yesterday()
    {
        $data['title'] = "Laporan Pengiriman Kemarin";
        $data['url'] = base_url('report/show/yesterday');
		$this->template->load('layout_dashboard', 'page_dashboard/report', $data);
    }

    public function show($x)
    {
        $time = $x;
        $key = $this->dataload->option('generate-key')->label;
		
		$data = array(
			"key" => "$key",
			"time_choose" => "$time"
		);
		$post_url = 'report_json';

		$data_string = json_encode($data);
		$ch = curl_init('http://116.203.92.59/api/' . $post_url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_VERBOSE, 0);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 360);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt(
			$ch,
			CURLOPT_HTTPHEADER,
			array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($data_string)
			)
		);
		$res = curl_exec($ch);
        curl_close($ch);
        echo $res;
        //exit;
    }
}
