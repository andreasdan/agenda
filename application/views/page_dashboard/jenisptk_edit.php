<form id="form_edit" name="form2" method="POST" enctype="multipart/form-data">
    <div class="modal-body">
        <input type="hidden" name="id" value="<?php echo en($e->jenis_ptk_id) ?>" type="text"
            class="form-control input-default" required>
        <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" value="<?php echo $e->nama_jenis_ptk ?>" class="form-control">
        </div>
    </div>
    <div class="modal-footer">
        <button type="reset" class="btn btn-outline-danger" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success">Simpan</button>
    </div>
</form>