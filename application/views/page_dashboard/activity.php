<main>
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<h1>Daftar <?php echo $title ?></h1>
				<div class="separator mb-5"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="card mb-4">
					<div class="card-body">
						<button type="button" class="btn btn-outline-primary" data-toggle="modal" data-backdrop="static" data-target="#modalCreate">+ Tambah Data</button>
						<br><br><br>
						<table id="datatable" class="table table-bordered table-hover">
							<thead>
								<tr align="center">
									<th>No</th>
									<th>Kode</th>
									<th>Label</th>
									<th>Induk dari</th>
									<th>Tipe</th>
									<th>Ikon</th>
									<th>Order</th>
									<th>Pilihan</th>
								</tr>
							</thead>
							<tbody>
								<?php
								$no = 1;
								foreach ($data as $d) :
									?>
									<tr>
										<td align="center"><?php echo $no++ ?></td>
										<td align="center"><b><?php echo $d->activity_id ?></b></td>
										<td><?php echo $d->label ?></td>
										<td align="center"><b><?php echo $d->parent ?></b></td>
										<td align="center"><?php echo $d->type ?></td>
										<td align="center"><i class="<?php echo $d->icon ?>" style="font-size: 30px;"></i></td>
										<td align="center"><?php echo $d->activity_order ?></td>
										<td width="20%" align="center">
											<a data-toggle="modal" data-backdrop="static" data-target="#modalUpdate_<?php echo md5($d->activity_id) ?>" class="btn btn-outline-secondary btn-xs mb-1">Edit</a>
											<a href="<?php echo base_url('activity/delete/' . en($d->activity_id)) ?>" class="delete-link btn btn-outline-danger btn-xs mb-1">Hapus</a>
										</td>
									</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

<!-- Modal Create -->
<div class="modal fade bd-example-modal-lg" id="modalCreate" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<form action="<?php echo base_url('activity/create') ?>" method="post">
				<div class="modal-header">
					<h5 class="modal-title">Input <?php echo $title ?></h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label>Induk dari</label>
						<select id="inputState" class="form-control" name="parent" required autofocus>
							<?php foreach ($parent as $e) : ?>
								<option value="<?php echo $e->activity_id ?>"><?php echo $e->label ?></option>
							<?php endforeach; ?>
						</select>
					</div>
					<div class="form-group">
						<label>Label</label>
						<input type="text" class="form-control" name="label" required autofocus>
					</div>
					<div class="form-group">
						<label>Type</label>
						<div class="custom-control custom-radio">
							<input type="radio" id="customRadio1" name="type" value="font" class="custom-control-input" checked>
							<label class="custom-control-label" for="customRadio1">Font</label>
						</div>
						<div class="custom-control custom-radio">
							<input type="radio" id="customRadio2" name="type" value="image" class="custom-control-input" disabled>
							<label class="custom-control-label" for="customRadio2">Image</label>
						</div>
					</div>
					<div class="form-group">
						<label>Icon</label>
						<input type="text" class="form-control" name="icon" required autofocus>
					</div>
					<div class="form-group">
						<label>Order</label>
						<input type="number" class="form-control" name="order" autofocus>
					</div>
				</div>
				<div class="modal-footer">
					<button type="reset" class="btn btn-outline-danger" data-dismiss="modal">Batal</button>
					<button type="submit" class="btn btn-primary">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>

<!-- Modal Update -->
<?php foreach ($data as $d) : ?>
	<div class="modal fade bd-example-modal-lg" id="modalUpdate_<?php echo md5($d->activity_id) ?>" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<form action="<?php echo base_url('activity/update') ?>" method="post">
					<div class="modal-header">
						<h5 class="modal-title">Ubah <?php echo $title ?></h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<input type="text" name="_key" value="<?php echo en($d->activity_id) ?>" hidden required autofocus>
						<div class="form-group">
							<label>Parent (sub dari)</label>
							<select class="form-control" name="parent" required autofocus>
								<option value="<?php echo $d->parent ?>" selected>Pilihan Anda : <?php echo $d->parent ?></option>
								<?php foreach ($parent as $e) :	?>
									<option value="<?php echo $e->parent ?>"><?php echo $e->label ?></option>
								<?php endforeach; ?>
							</select>
						</div>
						<div class="form-group">
							<label>Label</label>
							<input type="text" class="form-control" name="label" value="<?php echo $d->label ?>" required autofocus>
						</div>
						<div class="form-group">
							<label>Type</label>
							<div class="custom-control custom-radio">
								<input type="radio" id="customRadio1" name="type" value="font" class="custom-control-input" <?php if ($d->type == "font") {
																																	echo "checked";
																																} ?>>
								<label class="custom-control-label" for="customRadio1">Font</label>
							</div>
							<div class="custom-control custom-radio">
								<input type="radio" id="customRadio2" name="type" value="image" class="custom-control-input" <?php if ($d->type == "image") {
																																		echo "checked";
																																	} ?> disabled>
								<label class="custom-control-label" for="customRadio2">Image</label>
							</div>
						</div>
						<div class="form-group">
							<label>Icon</label>
							<input type="text" class="form-control" name="icon" value="<?php echo $d->icon ?>" required autofocus>
						</div>
						<div class="form-group">
							<label>Order</label>
							<input type="number" class="form-control" name="order" value="<?php echo $d->activity_order ?>" autofocus>
						</div>
					</div>
					<div class="modal-footer">
						<button type="reset" class="btn btn-outline-danger" data-dismiss="modal">Batal</button>
						<button type="submit" class="btn btn-primary">Simpan</button>
					</div>
				</form>
			</div>
		</div>
	</div>
<?php endforeach; ?>

<!-- Insert Alert -->
<?php if ($this->session->flashdata('insert_success')) : ?>
	<script type="text/javascript">
		swal({
			type: "success",
			title: "Data telah ditambahkan",
			showConfirmButton: false,
			timer: 2500
		})
	</script>
<?php endif; ?>

<!-- Update Alert -->
<?php if ($this->session->flashdata('update_success')) : ?>
	<script type="text/javascript">
		swal({
			type: "success",
			title: "Data telah diubah",
			showConfirmButton: false,
			timer: 2500
		})
	</script>
<?php endif; ?>

<!-- Delete Alert -->
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$('.delete-link').on('click', function() {
			var getLink = $(this).attr('href');
			swal({
				title: 'Hapus',
				text: 'Apakah Anda yakin akan menghapus data ini?',
				type: "info",
				html: true,
				confirmButtonColor: '#d9534f',
				showCancelButton: true,
			}, function() {
				window.location.href = getLink
			});
			return false;
		});
	});
</script>

<?php if ($this->session->flashdata('delete_success')) : ?>
	<script type="text/javascript">
		swal({
			type: "success",
			title: "Data telah dihapus",
			showConfirmButton: false,
			timer: 2500
		})
	</script>
<?php endif; ?>