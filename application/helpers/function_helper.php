<?php
// Copyright Footer
function copyright()
{
	date_default_timezone_set('Asia/Jakarta');
	$year = date("Y");
	$text = "&copy; " . $year . " SD Muhammadiah Sleman";
	return ($text);
}

function reject()
{
	redirect('auth/logout', 'refresh');
}

// Indonesian Date
function tanggal($x)
{
	if ($x == "0000-00-00" || $x == "0000-00-00 00:00:00") {
		return "-";
	} else {
		$tahun = substr($x, 0, 4);
		$bln = substr($x, 5, 2);
		$tgl = substr($x, 8, 2);
		if ($bln == 1) {
			$bulan = "Januari";
		} else if ($bln == 2) {
			$bulan = "Februari";
		} else if ($bln == 3) {
			$bulan = "Maret";
		} else if ($bln == 4) {
			$bulan = "April";
		} else if ($bln == 5) {
			$bulan = "Mei";
		} else if ($bln == 6) {
			$bulan = "Juni";
		} else if ($bln == 7) {
			$bulan = "Juli";
		} else if ($bln == 8) {
			$bulan = "Agustus";
		} else if ($bln == 9) {
			$bulan = "September";
		} else if ($bln == 10) {
			$bulan = "Oktober";
		} else if ($bln == 11) {
			$bulan = "November";
		} else {
			$bulan = "Desember";
		}
		return $date = $tgl . " " . $bulan . " " . $tahun;
	}
}

// Bulan
function bulan($x)
{
	if ($x == 1) {
		$a = "Januari";
	} else if ($x == 2) {
		$a = "Februari";
	} else if ($x == 3) {
		$a = "Maret";
	} else if ($x == 4) {
		$a = "April";
	} else if ($x == 5) {
		$a = "Mei";
	} else if ($x == 6) {
		$a = "Juni";
	} else if ($x == 7) {
		$a = "Juli";
	} else if ($x == 8) {
		$a = "Agustus";
	} else if ($x == 9) {
		$a = "September";
	} else if ($x == 10) {
		$a = "Oktober";
	} else if ($x == 11) {
		$a = "November";
	} else if ($x == 12) {
		$a = "Desember";
	} else {
		$a = "-";
	}
	return $a;
}

// Time now
function time_now()
{
	date_default_timezone_set('Asia/Jakarta');
	$time = date("H:i:s");
	return $time;
}

function jam($x)
{
	return substr($x,11,5);
}

// Rupiah
function rupiah($x)
{
	$text = "Rp " . number_format($x, 0, ',', '.');
	if ($x < 0) {
		return "<font color='red'>" . $text . "</font>";
	} else {
		return $text;
	}
}

// Currency terbilang (temp)
function kekata($x)
{
	$x = abs($x);
	$angka = array(
		"", "satu", "dua", "tiga", "empat", "lima",
		"enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas"
	);
	$temp = "";
	if ($x < 12) {
		$temp = " " . $angka[$x];
	} else if ($x < 20) {
		$temp = kekata($x - 10) . " belas";
	} else if ($x < 100) {
		$temp = kekata($x / 10) . " puluh" . kekata($x % 10);
	} else if ($x < 200) {
		$temp = " seratus" . kekata($x - 100);
	} else if ($x < 1000) {
		$temp = kekata($x / 100) . " ratus" . kekata($x % 100);
	} else if ($x < 2000) {
		$temp = " seribu" . kekata($x - 1000);
	} else if ($x < 1000000) {
		$temp = kekata($x / 1000) . " ribu" . kekata($x % 1000);
	} else if ($x < 1000000000) {
		$temp = kekata($x / 1000000) . " juta" . kekata($x % 1000000);
	} else if ($x < 1000000000000) {
		$temp = kekata($x / 1000000000) . " milyar" . kekata(fmod($x, 1000000000));
	} else if ($x < 1000000000000000) {
		$temp = kekata($x / 1000000000000) . " trilyun" . kekata(fmod($x, 1000000000000));
	}
	return $temp;
}

// Currency terbilang
function terbilang($x, $style = 4)
{
	if ($x < 0) {
		$hasil = "minus " . trim(kekata($x));
	} else {
		$hasil = trim(kekata($x));
	}
	switch ($style) {
		case 1:
			$hasil = strtoupper($hasil);
			break;
		case 2:
			$hasil = strtolower($hasil);
			break;
		case 3:
			$hasil = ucwords($hasil);
			break;
		default:
			$hasil = ucfirst($hasil);
			break;
	}
	return $hasil . " Rupiah";
}

//$string = "i like to program in PHP";
//$a = strtoupper($string);
//$b = strtolower($string);
//$c = ucfirst($string);
//$d = ucwords($string);
//$e = ucwords(strtolower($string));

// level user
function level($x)
{
	if ($x == 1) {
		$a = "Master";
	} else if ($x == 2) {
		$a = "Pengguna";
	} else {
		$a = "";
	}
	return $a;
}

// level user
function department($x)
{
	if ($x == 1) {
		$a = "Guru";
	} else if ($x == 2) {
		$a = "Karyawan";
	} else {
		$a = "-";
	}
	return $a;
}

// Agama
function text_agama($x)
{
	if ($x == 1) {
		$a = "Islam";
	} else if ($x == 2) {
		$a = "Kristen";
	} else if ($x == 3) {
		$a = "Katolik";
	} else if ($x == 4) {
		$a = "Hindu";
	} else if ($x == 5) {
		$a = "Budha";
	} else if ($x == 6) {
		$a = "Konghucu";
	} else {
		$a = "-";
	}
	return $a;
}

// Pendidikan
function text_pendidikan($x)
{
	if ($x == 1) {
		$a = "SD";
	} else if ($x == 2) {
		$a = "SLTP";
	} else if ($x == 3) {
		$a = "SLTA";
	} else if ($x == 4) {
		$a = "D3";
	} else if ($x == 5) {
		$a = "S1";
	} else if ($x == 6) {
		$a = "S2";
	} else {
		$a = "-";
	}
	return $a;
}

// Jenis Kelamin
function text_gender($x)
{
	if ($x == "L") {
		$a = "Laki-laki";
	} else if ($x == "P") {
		$a = "Perempuan";
	} else {
		$a = "-";
	}
	return $a;
}

// Kunjungan
function text_jeniskunjungan($x)
{
	if ($x == 1) {
		$a = "Umum";
	} else if ($x == 2) {
		$a = "KIA";
	} else if ($x == 3) {
		$a = "Kebidanan";
	} else if ($x == 4) {
		$a = "KB";
	} else if ($x == 5) {
		$a = "IGD";
	} else {
		$a = "-";
	}
	return $a;
}

// Status Pasien
function text_statuspasien($x)
{
	if ($x == 1) {
		$a = "Kawin";
	} else if ($x == 2) {
		$a = "Belum Kawin";
	} else if ($x == 3) {
		$a = "Janda";
	} else if ($x == 4) {
		$a = "Duda";
	} else {
		$a = "-";
	}
	return $a;
}

// Jenis Pasien
function text_jenispasien($x)
{
	if ($x == 1) {
		$a = "Mandiri";
	} else if ($x == 2) {
		$a = "BPJS";
	} else if ($x == 3) {
		$a = "Asuransi";
	} else {
		$a = "-";
	}
	return $a;
}

// Pekerjaan
function text_pekerjaan($x)
{
	if ($x == 1) {
		$a = "Pelajar";
	} else if ($x == 2) {
		$a = "PNS";
	} else if ($x == 3) {
		$a = "TNI";
	} else if ($x == 4) {
		$a = "Polri";
	} else if ($x == 5) {
		$a = "Petani";
	} else if ($x == 6) {
		$a = "Peternak";
	} else if ($x == 7) {
		$a = "Wirausaha";
	} else if ($x == 8) {
		$a = "Lainnya";
	} else {
		$a = "-";
	}
	return $a;
}

// Sweetalert Success
function alert_sucess()
{
	'<script type="text/javascript"> swal({type: "success",title: "Data Berhasil Dihapus",showConfirmButton: false,})</script>';
}
