<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Receiver extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('login_admin') == FALSE) {
			reject();
		}

		$this->load->model('datareceiver');
		$this->load->helper('url');
		$this->load->library('encrypt');
	}

	public function index()
	{
		$data['title'] = "Penerima Pesan"; //content page
		$data['data'] = $this->datareceiver->get_all()->result();

		$this->template->load('layout_dashboard', 'page_dashboard/receiver', $data);
	}

	public function edit()
    {
        $id = de($this->input->post('id'));
        $data['e'] = $this->datareceiver->get_id($id)->row();

        $this->load->view('page_dashboard/receiver_edit', $data); //layout
    }

	public function create()
	{
		$id_number = $this->input->post('id_number');
		$receiver_name = $this->input->post('receiver_name');
		$phone_number = $this->input->post('phone_number');
		$grade = $this->input->post('grade');
		$department = $this->input->post('department');

		// foreach (array_combine($name, $number) as $a => $b) {
		// 	$data = array(
		// 		'receiver_name' => $a,
		// 		'phone_number' => '62' . ltrim($b, '0') //menambahkan 62 kode nomor Indonesia dan menghilangkan 0 di depan inputan data
		// 	);
		// 	$this->datareceiver->add($data);
		// }

		foreach ($receiver_name as $key => $val) {
			$record = array(
				'id_number' => $id_number[$key],
				'receiver_name' => $receiver_name[$key],
				'phone_number' => '62' . ltrim($phone_number[$key], '0'),
				'department' => $department[$key],
			);
			$this->datareceiver->add($record);
		}
		$this->session->set_flashdata('insert_success', 'Success!');
		redirect('receiver');
		echo 'success!';
	}

	public function update()
	{
		$encrypt_id = $this->input->post('id');
		$id = de($encrypt_id);

		$record = array(
			'id_number' => $this->input->post('edit_id_number'),
			'receiver_name' => $this->input->post('edit_receiver_name'),
			'phone_number' => $this->input->post('edit_phone_number'),
			'department' => $this->input->post('edit_department')
		);

		$where = array(
			'receiver_id' => $id
		);

		$this->datareceiver->update($where, $record);
		$this->session->set_flashdata('update_success', 'Success!');
		redirect('receiver');
	}

	public function delete()
    {
        $id = de($this->input->post('id'));
        $where = array(
            'receiver_id' => $id
        );
        $data = $this->datareceiver->delete($where);
        echo json_encode($data);
    }

	public function delete_multiple()
	{
		$id = $this->input->post('receivers_ids');

		foreach ($id as $key => $val) {
			$where = array(
				'receiver_id' => $id[$key],
			);
			$this->datareceiver->delete($where);
		}
		echo "Success!";
		exit;
	}
}
