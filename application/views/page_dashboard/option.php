<div class="m-content">
    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30"
        role="alert">
        <div class="m-alert__icon">
            <i class="flaticon-questions-circular-button m--font-brand"></i>
        </div>
        <div class="m-alert__text">
            Halaman ini digunakan untuk mengubah pengaturan data.
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        <?php echo $title ?>
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">

            <?php foreach ($data as $d) : ?>
            <form action="<?php echo base_url('option/update')?>" method="post">
                <div class="form-group m-form__group">
                    <input type="text" name="_key" value="<?php echo en($d->option_id) ?>" hidden>
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <button class="btn btn-warning" type="button"><?php echo $d->name ?></button>
                        </div>
                        <input type="text" class="form-control" name="label" autocomplete="off" value="<?php echo $d->label ?>">
                        <div class="input-group-append">
                            <button type="submit" class="btn btn-success" type="button">Simpan</button>
                        </div>
                    </div>
                </div>
            </form>
            <?php endforeach; ?>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>


<!-- Update Alert -->
<?php if ($this->session->flashdata('update_success')) : ?>
<script type="text/javascript">
swal({
    type: "success",
    title: "Data telah diubah",
    showConfirmButton: false,
    timer: 2500
})
</script>
<?php endif; ?>

<?php if ($this->session->flashdata('image_error')) : ?>
<script type="text/javascript">
swal({
    type: "error",
    title: "Foto tidak valid",
    text: "Coba ganti dengan file lainnya",
    confirmButtonColor: '#d9534f',
    timer: 30000
})
</script>
<?php endif; ?>