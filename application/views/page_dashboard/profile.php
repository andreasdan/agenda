<div class="m-content">
    <div class="row">
        <div class="col-xl-3 col-lg-4">
            <div class="m-portlet m-portlet--full-height  ">
                <div class="m-portlet__body">
                    <div class="m-card-profile">
                        <div class="m-card-profile__title m--hide">
                            Profile
                        </div>
                        <div class="m-card-profile__pic">
                            <a href="#" class="m-card-profile__pic-wrapper" data-toggle="modal" data-backdrop="static" data-target="#modalImage">
                                <img src="<?php echo base_url() ?>uploads/profile/<?php echo $image ?>" onerror="this.src='<?php echo base_url() ?>assets/data/user.jpg'" alt=" " />
                            </a>
                        </div>
                        <div class="m-card-profile__details">
                            <a href="" class="m-card-profile__email m-link"><?php echo $code ?></a><br>
                            <b class=""><?php echo $name ?></b><br>
                            <a href="" class="m-card-profile__email m-link">@<?php echo $username ?></a>
                        </div>
                    </div>
                    <ul class="m-nav m-nav--hover-bg m-portlet-fit--sides">

                    </ul>
                </div>
            </div>
        </div>
        <div class="col-xl-9 col-lg-8">
            <div class="m-portlet m-portlet--full-height m-portlet--tabs  ">
                <div class="m-portlet__head">
                    <div class="m-portlet__head-tools">
                        <ul class="nav nav-tabs m-tabs m-tabs-line   m-tabs-line--left m-tabs-line--primary" role="tablist">
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link active" data-toggle="tab" href="#m_user_profile_tab_1" role="tab">
                                    <i class="flaticon-share m--hide"></i>
                                    Update Profile
                                </a>
                            </li>
                            <li class="nav-item m-tabs__item">
                                <a class="nav-link m-tabs__link" data-toggle="tab" href="#m_user_profile_tab_2" role="tab">
                                    Update Password
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="tab-content">
                    <div class="tab-pane active" id="m_user_profile_tab_1">
                        <form id="form_edit" class="m-form m-form--fit m-form--label-align-right">
                            <div class="m-portlet__body">
                                <div class="form-group m-form__group row">
                                </div>
                                <input type="hidden" id="id_" value="<?php echo en($id) ?>" type="text" required>
                                <div class="form-group m-form__group row">
                                    <label class="col-4 col-form-label">Nama</label>
                                    <div class="col-8">
                                        <input id="name_" value="<?php echo $name ?>" type="text" class="form-control m-input" maxlength="70" required autocomplete="off" disabled>
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-4 col-form-label">Username</label>
                                    <div class="col-8">
                                        <input id="username_" value="<?php echo $username ?>" type="text" class="form-control m-input" maxlength="70" required autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group m-form__group row">
                                    <label class="col-4 col-form-label"></label>
                                    <div class="col-8">
                                        <code>Klik foto profil untuk mengganti gambar.</code>
                                    </div>
                                </div>
                                <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit">
                                <div class="m-form__actions">
                                    <div class="row">
                                        <div class="col-2">
                                        </div>
                                        <div class="col-7">
                                            <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">Simpan Perubahan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane " id="m_user_profile_tab_2">
                        <form id="form_password" class="m-form m-form--fit m-form--label-align-right">
                            <div class="m-portlet__body">
                                <div class="form-group m-form__group row">
                                </div>
                                <input type="hidden" id="id_" value="<?php echo en($id) ?>" type="text" required>
                                <div class="form-group m-form__group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Ubah Password</label>
                                    <div class="col-7">
                                        <input id="password_" type="password" class="form-control m-input" maxlength="25" required>
                                        <span class="m-form__help">Kosongi apabila password tidak ingin diperbarui.</span>
                                    </div>
                                </div>
                                <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space-2x"></div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit">
                                <div class="m-form__actions">
                                    <div class="row">
                                        <div class="col-2">
                                        </div>
                                        <div class="col-7">
                                            <button type="submit" class="btn btn-accent m-btn m-btn--air m-btn--custom">Simpan Perubahan</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Image -->
<div class="modal fade bd-example-modal-lg" id="modalImage" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form action="<?php echo $url_image ?>" method="post" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title">Ubah <?php echo $title ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="text" name="_key" value="<?php echo en($id) ?>" hidden required autofocus>
                    <input type="text" name="old_image" value="<?php echo $image ?>" hidden>
                    <div class="form-group">
                        <input type="file" name="file_image">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="reset" class="btn btn-outline-danger" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-success">Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Update -->
<script type="text/javascript">
    $(document).ready(function() {


        // proses untuk mengubah data
        $('#form_edit').on('submit', function() {
            var tablenya = $('.table');

            var id = $('#id_').val();
            var name = $('#name_').val();
            var username = $('#username_').val();
            //Perubahan
            tablenya.find('tr:eq(0) > td:eq(2)').text(name)
            tablenya.find('tr:eq(1) > td:eq(2)').text(username)

            $.ajax({
                type: "post",
                url: "<?php echo $url_identity ?>",
                data: {
                    id: id,
                    name: name,
                    username: username,
                }, // ambil datanya dari form yang ada di variabel
                success: function(data) {
                    //location.reload();
                    // Toast sukses
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                        onOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })
                    Toast.fire({
                        type: 'success',
                        title: 'Menyunting <?php echo $title ?>'
                    })
                    // bersihkan form pada modal
                    $('#modal_form_edit').modal('hide');
                }
            })
            return false;
        });
        $('#form_password').on('submit', function() {
            var tablenya = $('.table');

            var id = $('#id_').val();
            var password = $('#password_').val();

            $.ajax({
                type: "post",
                url: "<?php echo $url_password ?>",
                data: {
                    id: id,
                    password: password,
                }, // ambil datanya dari form yang ada di variabel
                success: function(data) {
                    //location.reload();
                    // Toast sukses
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                        onOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })
                    Toast.fire({
                        type: 'success',
                        title: 'Menyunting Password'
                    })
                    $("#form_password")[0].reset();
                }
            })
            return false;
        });
    });
</script>