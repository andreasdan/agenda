<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Agenda extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('login_admin') == FALSE) {
			reject();
		}

		$this->load->model('dataagenda');
		$this->load->library('encryption');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
	}

	public function index()
	{
		$data['title'] = "Agenda"; //title
		$data['url'] = base_url('agenda/all'); //url

		$this->template->load('layout_dashboard', 'page_dashboard/agenda', $data);
	}

	public function all()
    {
        $record = $this->dataagenda->get_all()->result();
        $no = 1;
        foreach ($record as $d) {
            $tbody = array();
            $tbody[] = $no++;
            $tbody[] = $d->nama_agenda;
            $tbody[] = tanggal($d->tanggal_agenda);
            $tbody[] = jam($d->tanggal_agenda).' WIB';
            $tbody[] = $d->tempat;
            $tbody[] = $d->keterangan;
            $tbody[] = '<span class="dropdown show">
			<a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true"><i class="la la-ellipsis-h"></i></a>
			<div class="dropdown-menu dropdown-menu-right" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-196px, -24px, 0px);">
				<button data-id=' . en($d->agenda_id) . ' data-toggle="modal" class="edit-data dropdown-item"><i class="la la-edit"></i> Edit</button>
				<a href="'.base_url('select/all/' . en($d->agenda_id)).'" class="dropdown-item"><i class="la la-send"></i> Kirim</a>
				<button data-id=' . en($d->agenda_id) . ' data-toggle="modal" class="delete-data dropdown-item" ><i class="la la-trash"></i> Hapus</button>
			</div>
			</span>';
            $data[] = $tbody;
        }

        if ($record) {
            echo json_encode(array('data' => $data));
        } else {
            echo json_encode(array('data' => 0));
        }
	}
	
	public function edit()
    {
        $id = de($this->input->post('id'));
        $data['e'] = $this->dataagenda->get_id($id)->row();

        $this->load->view('page_dashboard/agenda_edit', $data); //layout
    }

	public function create()
	{
		$record = array(
			'nama_agenda' => $this->input->post('nama'),
			'tanggal_agenda' => $this->input->post('tanggal'),
			'tempat' => $this->input->post('tempat'),
			'keterangan' => $this->input->post('keterangan'),
		);
		$data = $this->dataagenda->add($record);
		echo json_encode($data);
    }
    
    public function update()
    {
        $id = de($this->input->post('id'));
        $record = array(
            'nama_agenda' => $this->input->post('nama'),
			'tanggal_agenda' => $this->input->post('tanggal'),
			'tempat' => $this->input->post('tempat'),
			'keterangan' => $this->input->post('keterangan'),
        );
        $where = array(
            'agenda_id' => $id
        );

        $data = $this->dataagenda->update($where, $record);
        echo json_encode($data);
    }

	public function delete()
    {
        $id = de($this->input->post('id'));
        $where = array(
            'agenda_id' => $id
        );
        $data = $this->dataagenda->delete($where);
        echo json_encode($data);
    }
}