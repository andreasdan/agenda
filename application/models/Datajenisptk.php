<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Datajenisptk extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}

	public function get_all()
	{
		$this->db->from("jenis_ptk");
		$this->db->order_by("jenis_ptk_id", "asc");
		return $this->db->get();
	}

	public function get_id($x)
	{
		$this->db->where("jenis_ptk_id", $x);
		return $this->db->get("jenis_ptk");
	}

	public function add($data)
	{
		return $this->db->insert('jenis_ptk', $data);
	}

	public function edit($x, $y)
	{
		$this->db->where($x, $y);
		return $this->db->get("jenis_ptk");
	}

	public function update($where, $data)
	{
		$this->db->where($where);
		$this->db->update('jenis_ptk', $data);
	}

	function delete($where)
	{
		$this->db->where($where);
		$this->db->delete('jenis_ptk');
    }
}