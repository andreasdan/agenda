<form id="form_edit" name="form2" method="POST" enctype="multipart/form-data">
    <div class="modal-body">
        <div>
            <input type="hidden" name="id" value="<?php echo en($e->receiver_id) ?>" type="text"
                class="form-control input-default" required>
            <div class="form-group">
                <label>NIK</label>
                <input type="text" class="form-control" name="edit_id_number" value="<?php echo $e->id_number ?>"
                    required>
            </div>
            <div class="form-group">
                <label>Nama</label>
                <input type="text" class="form-control" name="edit_receiver_name"
                    value="<?php echo $e->receiver_name ?>" required>
            </div>
            <div class="form-group">
                <label>Nomor HP</label>
                <input type="text" class="form-control" name="edit_phone_number" value="<?php echo $e->phone_number ?>"
                    required>
            </div>
            <div class="form-group">
                <label>Department</label>
                <select class="mySelect form-control" name="edit_department" required>
                    <option value="">- Departement -</option>
                    <option value="1" <?php echo ($e->department == '1') ? 'selected' : '' ?>>
                        <?php echo department(1) ?></option>
                    <option value="2" <?php echo ($e->department == '2') ? 'selected' : '' ?>>
                        <?php echo department(2) ?></option>
                </select>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="reset" class="btn btn-outline-danger" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success">Simpan</button>
    </div>
</form>