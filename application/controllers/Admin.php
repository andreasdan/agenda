<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('login_admin') == FALSE) {
			reject();
		}

        $this->load->model('dataadmin');
        $this->load->library('encryption');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
    }

    public function index()
    {
        $data['title'] = "Admin"; //title
        $data['url'] = base_url('admin/all'); //url

        $this->template->load('layout_dashboard', 'page_dashboard/admin', $data);
    }

    public function all()
    {
        $record = $this->dataadmin->get_all()->result();
        $no = 1;
        foreach ($record as $d) {
            $tbody = array();
            $tbody[] = $no++;
            $tbody[] = $d->nama;
            $tbody[] = $d->username;
            $tbody[] = '<span class="dropdown show">
			<a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true"><i class="la la-ellipsis-h"></i></a>
			<div class="dropdown-menu dropdown-menu-right" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-196px, -24px, 0px);">
				<button data-id=' . en($d->admin_id) . ' data-toggle="modal" class="edit-data dropdown-item"><i class="la la-edit"></i> Edit</button>
				<button data-id=' . en($d->admin_id) . ' data-toggle="modal" class="delete-data dropdown-item" ><i class="la la-trash"></i> Hapus</button>
			</div>
			</span>';
            $data[] = $tbody;
        }

        if ($record) {
            echo json_encode(array('data' => $data));
        } else {
            echo json_encode(array('data' => 0));
        }
    }

    public function edit()
    {
        $id = de($this->input->post('id'));
        $data['e'] = $this->dataadmin->get_id($id)->row();

        $this->load->view('page_dashboard/admin_edit', $data); //layout
    }

    public function create()
    {
        $record = array(
            'nama' => $this->input->post('nama'),
            'username' => $this->input->post('username'),
            'password' => md5($this->input->post('password')),
        );
        $data = $this->dataadmin->insert($record);
        echo json_encode($data);
    }

    public function update()
    {
        $id = de($this->input->post('id'));
        $password = $this->input->post('password');
        $record = array(
            'nama' => $this->input->post('nama'),
            'username' => $this->input->post('username'),
        );
        if ($password !== "") {
            $record['password'] = md5($password);
        }
        $where = array(
            'admin_id' => $id
        );

        $data = $this->dataadmin->update($where, $record);
        echo json_encode($data);
    }

    public function update_image()
    {
        $encrypt_id = $this->input->post('_key');
        $id = de($encrypt_id);

        // memanggil private function untuk upload gambar
        $fileimage = $this->upload_image();

        // mendapatkan nama file foto
        $get_image = $fileimage['file_name'];

        // delete old image
        $old_image = $this->input->post('old_image');
        if (file_exists('uploads/profile/' . $old_image) && $old_image)
            unlink('uploads/profile/' . $old_image);

        $record = array(
            'image' => $get_image
        );
        $where = array(
            'admin_id' => $id
        );

        $this->dataadmin->update($where, $record);
        $this->session->set_flashdata('update_success', 'Success!');
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function identity_update()
    {
        $id = de($this->input->post('id'));
        $record = array(
            'username' => $this->input->post('username')
        );
        $where = array(
            'admin_id' => $id
        );

        $data = $this->dataadmin->update($where, $record);
        echo json_encode($data);
    }

    public function password_update()
    {
        $id = de($this->input->post('id'));
        $record = array(
            'password' => md5($this->input->post('password')),
        );
        $where = array(
            'admin_id' => $id
        );

        $data = $this->dataadmin->update($where, $record);
        echo json_encode($data);
    }

    public function delete()
    {
        $id = de($this->input->post('id'));
        $where = array(
            'admin_id' => $id
        );
        $data = $this->dataadmin->delete($where);
        echo json_encode($data);
    }

    //Private
    private function upload_image()
    {
        // Setup folder upload path
        $config['upload_path']        = './uploads/profile/';
        // Setup file yang di izinkan
        $config['allowed_types']    = 'gif|jpg|png|jpeg';
        // Encrpt nama foto agar tidak sama
        $config['encrypt_name']        = TRUE;
        // Memanggil library upload disertai dengan paramter $config array
        $this->load->library('upload', $config);
        // jika upload gagal, return false
        if ($this->upload->do_upload('file_image') == false) {
            return false;
            #return $this->upload->display_errors();
        }
        // jika upload berhasil, return nama file dan membuat thumbnail foto
        else {
            // Mengambil data yang berhasil di upload
            $uploaded_data = $this->upload->data();
            // Mendapatkan nama file
            $file_name = $uploaded_data['file_name'];
            // Memanggil library GD 2
            $config['image_library'] = 'gd2';
            // Memanggil nama file images
            $config['source_image'] = './uploads/profile/' . $file_name;
            // Membuat thumbnail
            $config['create_thumb'] = FALSE;
            // Mempertahankan foto berdasarkan ratio, hal ini digunakan agar foto tidak gepeng
            $config['maintain_ratio'] = TRUE;
            // Setting lebar dan tinggi
            $config['width']         = 200;
            //$config['height']       = 100;
            // Memanggil library image_lib untuk memproses images resize disertai dengan parameter $config
            $this->load->library('image_lib', $config);
            // Melakukan resize
            $this->image_lib->resize();
            // Return data
            return $uploaded_data;
        }
    }
}
