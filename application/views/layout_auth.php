<!DOCTYPE html>
<html lang="en">
<!-- begin::Head -->

<head>
    <meta charset="utf-8" />
    <title><?php echo $title ?></title>
    <meta name="description" content="SD Muhammadiah Sleman Yogyakarta">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
        WebFont.load({
            google: {
                "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
            },
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <link href="<?php echo base_url() ?>assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/vendors/sweetalert/css/sweetalert.css" rel="stylesheet">
    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/data/favicon.ico" />
</head>

<!-- end::Head -->

<!-- begin::Body -->

<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--hor m-login m-login--signin m-login--2 m-login-5--skin-3" id="m_login" style="background-color:powderblue;">
            <div class="m-grid__item m-grid__item--fluid m-login__wrapper">
                <div class="m-login__container">
                    <div class="m-login__logo">
                        <a href="#">
                            <img src="<?php echo base_url() ?>assets/data/logo.png" alt=" " width="100">
                        </a>
                    </div>
                    <div class="m-login__signin">
                        <div class="m-login__head">
                            <h6 class="m--align-center">Disposisi Surat</h6>
                            <h3 class="m-login__title">SD Muhammadiah Sleman Yogyakarta</h3>
                        </div>
                        <form class="m-login__form m-form" method="post" action="<?php echo base_url('auth/login') ?>">
                            <div class="form-group m-form__group">
                                <input class="form-control m-input" type="text" placeholder="username" name="username" autocomplete="off" autofocus required>
                            </div>
                            <div class="form-group m-form__group">
                                <input class="form-control m-input m-login__form-input--last" type="password" placeholder="password" name="password" required>
                            </div>
                            <div class="row m-login__form-sub">
                                <!-- <div class="col m--align-left m-login__form-left">
                                    <label class="m-checkbox  m-checkbox--light">
                                        <input type="checkbox" name="remember"> Remember me
                                        <span></span>
                                    </label>
                                </div> -->
                                <div class="col m--align-center">
                                    <!-- <a href="javascript:;" id="m_login_forget_password" class="m-link m-link--light">Lupa Password ? Hubungi administrator di <b><a href=""> sini</a></b></a> -->
                                </div>
                            </div>
                            <div class="m-login__form-action">
                                <button id="m_login_signin_submit" class="btn btn-primary m-btn m-btn--pill m-btn--custom m-btn--air  m-login__btn">Masuk</button>
                            </div>
                        </form>
                    </div>
                    <div class="m-login__account">
                        <span class="m-login__account-msg">
                            <?php echo copyright() ?>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- end:: Page -->

    <!--begin::Global Theme Bundle -->
    <script src="<?php echo base_url() ?>assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>

    <!--end::Global Theme Bundle -->

    <!--begin::Page Scripts -->
    <script src="<?php echo base_url() ?>assets/snippets/custom/pages/user/login6.js" type="text/javascript"></script>

    <script src="<?php echo base_url() ?>assets/vendors/sweetalert/js/sweetalert.min.js"></script>
    <script src="<?php echo base_url() ?>assets/vendors/sweetalert/js/sweetalert.init.js"></script>

    <?php if ($this->session->flashdata('login_success')) : ?>
        <script type="text/javascript">
            swal({
                type: "success",
                title: "Login berhasil...",
                text: "Selamat datang di halaman dashboard",
                showConfirmButton: false,
                timer: 25000
            })
        </script>
    <?php endif; ?>
    <?php if ($this->session->flashdata('login_failed')) : ?>
        <script type="text/javascript">
            swal({
                type: "error",
                title: "Login gagal...",
                text: "Kombiasi username dan password salah!",
                confirmButtonColor: '#f27474',
                timer: 30000
            })
        </script>
    <?php endif; ?>

    <!--end::Page Scripts -->
</body>

<!-- end::Body -->

</html>