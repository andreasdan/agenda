<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Select extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('login_admin') == FALSE) {
			reject();
		}

        $this->load->model('dataagenda');
        $this->load->model('dataptk');
		$this->load->model('datajenisptk');
        $this->load->library('encryption');
        $this->load->helper(array('form', 'url'));
        $this->load->library('form_validation');
    }

    public function index()
    {
        echo "";
    }

    public function all($encrypt_id)
    {
        $agenda_id = de($encrypt_id);
        $data['title'] = "Semua"; //title
        $data['data'] = $this->dataagenda->get_id($agenda_id)->result();
        $data['ptk'] = $this->dataptk->get_all()->result();
        $data['jenis_ptk'] = $this->datajenisptk->get_all()->result();

        $this->template->load('layout_dashboard', 'page_dashboard/agenda_select', $data);
    }

    public function jenisptk($encrypt_id,$x)
    {
        $agenda_id = de($encrypt_id);
        $jenis_ptk_id = $x;
        $data['title'] = $this->datajenisptk->get_id($jenis_ptk_id)->row()->nama_jenis_ptk;
        $data['data'] = $this->dataagenda->get_id($agenda_id)->result();
        $data['ptk'] = $this->dataptk->get_jenis_ptk($jenis_ptk_id)->result();
        $data['jenis_ptk'] = $this->datajenisptk->get_all()->result();

        $this->template->load('layout_dashboard', 'page_dashboard/agenda_select', $data);
    }
}
