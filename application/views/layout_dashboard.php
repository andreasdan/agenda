<?php
    $id = $this->session->userdata('id');
    $user = $this->dataload->user_session($id);
    foreach ($user as $d) {
        $session_name = $d->nama;
        $session_username = $d->username;
        $session_image = '';
    }
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title><?php echo $title ?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">

    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script>
    WebFont.load({
        google: {
            "families": ["Poppins:300,400,500,600,700", "Roboto:300,400,500,600,700"]
        },
        active: function() {
            sessionStorage.fonts = true;
        }
    });
    </script>
    <link href="<?php echo base_url() ?>assets/vendors/base/vendors.bundle.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/demo/default/base/style.bundle.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url() ?>assets/vendors/custom/datatables/datatables.bundle.css" rel="stylesheet"
        type="text/css" />
    <link rel="shortcut icon" href="<?php echo base_url() ?>assets/data/favicon.ico" />
    <script src="<?php echo base_url() ?>assets/vendors/jquery/jquery.min.js"></script>    
</head>
<!-- end::Head -->
<!-- begin::Body -->

<body
    class="m-page--fluid m--skin- m-content--skin-light2 m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--fixed m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default">

    <!-- begin:: Page -->
    <div class="m-grid m-grid--hor m-grid--root m-page">

        <!-- BEGIN: Header -->
        <header id="m_header" class="m-grid__item    m-header " m-minimize-offset="200" m-minimize-mobile-offset="200">
            <div class="m-container m-container--fluid m-container--full-height">
                <div class="m-stack m-stack--ver m-stack--desktop">

                    <!-- BEGIN: Brand -->
                    <div class="m-stack__item m-brand  m-brand--skin-dark ">
                        <div class="m-stack m-stack--ver m-stack--general">
                            <div class="m-stack__item m-stack__item--middle m-brand__logo">
                                <a href="<?php echo base_url('dashboard') ?>" class="m-brand__logo-wrapper">
                                    <img alt="" src="<?php echo base_url() ?>assets/data/logo_secondary.png" />
                                </a>
                            </div>
                            <div class="m-stack__item m-stack__item--middle m-brand__tools">

                                <!-- BEGIN: Left Aside Minimize Toggle -->
                                <a href="javascript:;" id="m_aside_left_minimize_toggle"
                                    class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-desktop-inline-block  ">
                                    <span></span>
                                </a>
                                <!-- END -->

                                <!-- BEGIN: Responsive Aside Left Menu Toggler -->
                                <a href="javascript:;" id="m_aside_left_offcanvas_toggle"
                                    class="m-brand__icon m-brand__toggler m-brand__toggler--left m--visible-tablet-and-mobile-inline-block">
                                    <span></span>
                                </a>
                                <!-- END -->

                                <!-- BEGIN: Topbar Toggler -->
                                <a id="m_aside_header_topbar_mobile_toggle" href="javascript:;"
                                    class="m-brand__icon m--visible-tablet-and-mobile-inline-block">
                                    <i class="flaticon-more"></i>
                                </a>

                                <!-- BEGIN: Topbar Toggler -->
                            </div>
                        </div>
                    </div>

                    <!-- END: Brand -->
                    <div class="m-stack__item m-stack__item--fluid m-header-head" id="m_header_nav">

                        <!-- BEGIN: Horizontal Menu -->
                        <button class="m-aside-header-menu-mobile-close  m-aside-header-menu-mobile-close--skin-dark "
                            id="m_aside_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
                        <div id="m_header_menu"
                            class="m-header-menu m-aside-header-menu-mobile m-aside-header-menu-mobile--offcanvas  m-header-menu--skin-light m-header-menu--submenu-skin-light m-aside-header-menu-mobile--skin-dark m-aside-header-menu-mobile--submenu-skin-dark ">
                            <ul class="m-menu__nav  m-menu__nav--submenu-arrow ">
                            </ul>
                        </div>
                        <!-- END: Horizontal Menu -->

                        <!-- BEGIN: Topbar -->
                        <div id="m_header_topbar"
                            class="m-topbar  m-stack m-stack--ver m-stack--general m-stack--fluid">
                            <div class="m-stack__item m-topbar__nav-wrapper">
                                <ul class="m-topbar__nav m-nav m-nav--inline">
                                    <li class="m-nav__item m-topbar__user-profile m-topbar__user-profile--img  m-dropdown m-dropdown--medium m-dropdown--arrow m-dropdown--header-bg-fill m-dropdown--align-right m-dropdown--mobile-full-width m-dropdown--skin-light"
                                        m-dropdown-toggle="click">
                                        <a href="#" class="m-nav__link m-dropdown__toggle">
                                            <span class="m-topbar__userpic">
                                                <img src="<?php echo base_url() ?>uploads/profile/<?php echo $session_image ?>"
                                                    onerror="this.src='<?php echo base_url() ?>assets/data/user.jpg'"
                                                    alt=" " class="m--img-rounded m--marginless" />
                                            </span>
                                        </a>
                                        <div class="m-dropdown__wrapper">
                                            <span
                                                class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                            <div class="m-dropdown__inner">
                                                <div class="m-dropdown__header m--align-center"
                                                    style="background: url<?php echo base_url() ?>assets/app/media/img/misc/user_profile_bg.jpg); background-size: cover;">
                                                    <div class="m-card-user m-card-user--skin-dark">
                                                        <div class="m-card-user__pic">
                                                            <img src="<?php echo base_url() ?>uploads/profile/<?php echo $session_image ?>"
                                                                onerror="this.src='<?php echo base_url() ?>assets/data/user.jpg'"
                                                                alt=" " class=" m--img-rounded m--marginless" />

                                                            <!--<span class="m-type m-type--lg m--bg-danger"><span class="m--font-light">S<span><span>
						-->
                                                        </div>
                                                        <div class="m-card-user__details">
                                                            <span
                                                                class="m-card-user__name m--font-weight-500"><span><?php echo $session_name ?></span><br>
                                                                <a href=""
                                                                    class="m-card-user__email m--font-weight-300 m-link">@<?php echo $session_username ?></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="m-dropdown__body">
                                                    <div class="m-dropdown__content">
                                                        <ul class="m-nav m-nav--skin-light">
                                                            <li class="m-nav__item">
                                                                <a href="<?php echo base_url('profile') ?>"
                                                                    class="m-nav__link">
                                                                    <i class="m-nav__link-icon flaticon-profile-1"></i>
                                                                    <span class="m-nav__link-title">
                                                                        <span class="m-nav__link-wrap">
                                                                            <span class="m-nav__link-text">Setting
                                                                                Profile</span>
                                                                        </span>
                                                                    </span>
                                                                </a>
                                                            </li>
                                                            <!-- <li class="m-nav__item">
                                                                <a href="" class="m-nav__link">
                                                                    <i class="m-nav__link-icon flaticon-lifebuoy"></i>
                                                                    <span class="m-nav__link-text">Bantuan</span>
                                                                </a>
                                                            </li> -->
                                                            <li class="m-nav__separator m-nav__separator--fit">
                                                            </li>
                                                            <li class="m-nav__item">
                                                                <a href="<?php echo base_url('auth/logout') ?>"
                                                                    class="btn m-btn--pill    btn-secondary m-btn m-btn--custom m-btn--label-brand m-btn--bolder">Logout</a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <!-- END: Topbar -->
                    </div>
                </div>
            </div>
        </header>

        <!-- END: Header -->

        <!-- begin::Body -->
        <div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-body">

            <!-- BEGIN: Left Aside -->
            <button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i
                    class="la la-close"></i></button>
            <div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

                <!-- BEGIN: Aside Menu -->
                <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark "
                    m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
                    <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">
                        <li class="m-menu__item " aria-haspopup="true"><a href="" class="m-menu__link "><i
                                    class="m-menu__link-icon flaticon-user-ok"></i><span class="m-menu__link-title">
                                    <span class="m-menu__link-wrap"><span class="m-menu__link-text">Halaman
                                            Admin</span></span></span></a></li>
                        <li class="m-menu__section ">
                            <h4 class="m-menu__section-text">Menu</h4>
                            <i class="m-menu__section-icon flaticon-more-v2"></i>
                        </li>
                        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"
                            m-menu-submenu-toggle="hover"><a href="<?php echo base_url('agenda') ?>"
                                class="m-menu__link m-menu__toggle"><i
                                    class="m-menu__link-icon flaticon-paper-plane"></i><span
                                    class="m-menu__link-text">Agenda</span></a>
                        </li>
                        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"
                            m-menu-submenu-toggle="hover"><a href="<?php echo base_url('jenisptk') ?>"
                                class="m-menu__link m-menu__toggle"><i
                                    class="m-menu__link-icon flaticon-user-ok"></i><span
                                    class="m-menu__link-text">Jenis PTK</span></a>
                        </li>                     
                        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"
                            m-menu-submenu-toggle="hover"><a href="<?php echo base_url('ptk') ?>"
                                class="m-menu__link m-menu__toggle"><i
                                    class="m-menu__link-icon flaticon-user-ok"></i><span
                                    class="m-menu__link-text">PTK</span></a>
                        </li>
                        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"
                            m-menu-submenu-toggle="hover"><a href="<?php echo base_url('kirim') ?>"
                                class="m-menu__link m-menu__toggle"><i
                                    class="m-menu__link-icon flaticon-layers"></i><span
                                    class="m-menu__link-text">Arsip</span></a>
                        </li>
                        <li class="m-menu__item  m-menu__item--submenu" aria-haspopup="true"
                            m-menu-submenu-toggle="hover"><a href="<?php echo base_url('admin') ?>"
                                class="m-menu__link m-menu__toggle"><i
                                    class="m-menu__link-icon flaticon-users"></i><span
                                    class="m-menu__link-text">Admin</span></a>
                        </li>
                        <li class="m-menu__section ">
                            <h4 class="m-menu__section-text">Version 1.1</h4>
                            <i class="m-menu__section-icon flaticon-more-v2"></i>
                        </li>
                    </ul>
                </div>

                <!-- END: Aside Menu -->
            </div>

            <!-- END: Left Aside -->
            <div class="m-grid__item m-grid__item--fluid m-wrapper">
                <?php echo $contents; ?>
            </div>
        </div>

        <!-- end:: Body -->

        <!-- begin::Footer -->
        <footer class="m-grid__item		m-footer ">
            <div class="m-container m-container--fluid m-container--full-height m-page__container">
                <div class="m-stack m-stack--flex-tablet-and-mobile m-stack--ver m-stack--desktop">
                    <div class="m-stack__item m-stack__item--left m-stack__item--middle m-stack__item--last">
                        <span class="m-footer__copyright">
                            <p><?php echo copyright() ?></p>
                        </span>
                    </div>
                    <div class="m-stack__item m-stack__item--right m-stack__item--middle m-stack__item--first">
                        <ul class="m-footer__nav m-nav m-nav--inline m--pull-right">
                            <!-- <li class="m-nav__item">
                                <a href="#" class="m-nav__link">
                                    <span class="m-nav__link-text">About</span>
                                </a>
                            </li>
                            <li class="m-nav__item">
                                <a href="#" class="m-nav__link">
                                    <span class="m-nav__link-text">Privacy</span>
                                </a>
                            </li>
                            <li class="m-nav__item">
                                <a href="#" class="m-nav__link">
                                    <span class="m-nav__link-text">T&C</span>
                                </a>
                            </li>
                            <li class="m-nav__item">
                                <a href="#" class="m-nav__link">
                                    <span class="m-nav__link-text">Purchase</span>
                                </a>
                            </li> -->
                            <!-- <li class="m-nav__item m-nav__item">
                                <a href="#" class="m-nav__link" data-toggle="m-tooltip" title="Support Center" data-placement="left">
                                    <i class="m-nav__link-icon flaticon-info m--icon-font-size-lg3"></i>
                                </a>
                            </li> -->
                        </ul>
                    </div>
                </div>
            </div>
        </footer>

        <!-- end::Footer -->
    </div>

    <!-- end:: Page -->

    <!-- end::Quick Sidebar -->

    <!-- begin::Scroll Top -->
    <div id="m_scroll_top" class="m-scroll-top">
        <i class="la la-arrow-up"></i>
    </div>

    <!-- end::Scroll Top -->

    <!-- begin::Quick Nav -->

    <!--begin::Global Theme Bundle -->
    <script src="<?php echo base_url() ?>assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/demo/default/base/scripts.bundle.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/wa.js" type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/wa2.js" type="text/javascript"></script>    

    <!--end::Global Theme Bundle -->

    <!--begin::Page Vendors -->
    <script src="<?php echo base_url() ?>assets/vendors/custom/datatables/datatables.bundle.js" type="text/javascript">
    </script>

    <!--end::Page Vendors -->

    <!--begin::Page Scripts -->
    <script src="<?php echo base_url() ?>assets/demo/default/custom/crud/datatables/basic/paginations.js"
        type="text/javascript"></script>
    <script src="<?php echo base_url() ?>assets/demo/default/custom/crud/forms/widgets/select2.js"
        type="text/javascript"></script>

    <!--end::Page Scripts -->
    <!-- SweetAlert2 -->
    <script src="<?php echo base_url() ?>assets/demo/default/custom/components/base/sweetalert2.js"></script>
    <script src="<?php echo base_url() ?>assets/demo/default/custom/crud/forms/widgets/bootstrap-timepicker.js">
    <script src="<?php echo base_url() ?>assets/demo/default/custom/crud/forms/widgets/bootstrap-datetimepicker.js" type="text/javascript"></script>
    </script>

    <script>
    $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
        setDate: new Date(),
        todayHighlight: true,
        autoclose: true,
        language: 'id'
    });
    $(function () {
        $('.datetimepicker').datetimepicker();
    });
    </script>

    <!-- Login Success -->
    <?php if ($this->session->flashdata('login_success')) : ?>
    <script type="text/javascript">
    swal({
        type: "success",
        title: "Login berhasil...",
        text: "Selamat datang <?php echo $name ?>",
        showConfirmButton: false,
        timer: 2500
    })
    </script>
    <?php endif; ?>

    <!-- Insert Alert -->
    <?php if ($this->session->flashdata('insert_success')) : ?>
    <script type="text/javascript">
    swal({
        type: "success",
        title: "Data telah ditambahkan",
        showConfirmButton: false,
        timer: 2500
    })
    </script>
    <?php endif; ?>

    <!-- Update Alert -->
    <?php if ($this->session->flashdata('update_success')) : ?>
    <script type="text/javascript">
    swal({
        type: "success",
        title: "Data telah diubah",
        showConfirmButton: false,
        timer: 2500
    })
    </script>
    <?php endif; ?>

    <!-- Delete Alert -->
    <script type="text/javascript">
    jQuery(document).ready(function($) {
        $('.delete-link').on('click', function() {
            var getLink = $(this).attr('href');
            swal({
                title: 'Hapus',
                text: 'Apakah Anda yakin akan menghapus data ini?',
                type: "info",
                html: true,
                confirmButtonColor: '#d9534f',
                showCancelButton: true,
            }, function() {
                window.location.href = getLink
            });
            return false;
        });
    });
    </script>

    <?php if ($this->session->flashdata('delete_success')) : ?>
    <script type="text/javascript">
    swal({
        type: "success",
        title: "Data telah dihapus",
        showConfirmButton: false,
        timer: 2500
    })
    </script>
    <?php endif; ?>
</body>

<!-- end::Body -->

</html>