<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Send extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('level') !== '1') {
            reject();
        }

        $this->load->model('datamessage');
        $this->load->model('datareceiver');
        $this->load->library('encryption');
        $this->load->helper(array('form', 'url'));
    }

    public function index()
    {
        echo "Not Found";
    }

    public function wa()
    {
		$phone_no = $this->input->post('phone_no'); // Nomor Penerima
        $message_id = $this->input->post('message_id');
        $type = $this->input->post('type');
		$message = $this->datamessage->get_id($message_id)->row()->content; // Pesan
		$receiver = $this->datareceiver->get_phone($phone_no)->row()->receiver_name; // Pesan
		//$image = $this->datamessage->get_id($message_id)->row()->image; // Pesan
		$image = 'https://automatika.id/assets/img/automatika-black.png';
		$key = $this->dataload->option('generate-key')->label;
		

		$message = preg_replace("/(\n)/", "<ENTER>", $message);
		$message = preg_replace("/(\r)/", "<ENTER>", $message);

		$phone_no = preg_replace("/(\n)/", ",", $phone_no);
		$phone_no = preg_replace("/(\r)/", "", $phone_no);
		if (!empty($receiver)){
            $to = "Kepada *" . $receiver . "* <ENTER>"; 
        } else {
            $to = "";
        }
		if ($type == 'text_image' && !empty($image)) {
			$data = array(
				"phone_no" => $phone_no,
                "key" => "$key",
                "url" => base_url('uploads/' . $image),
				"message" => $to . $message . "<ENTER>*PT. Tirta Investama Klaten*"
			);
			$post_url = 'async_send_image_url';
		} else {
			$data = array(
				"phone_no" => $phone_no,
				"key" => "$key",
				"message" => $to . $message . "<ENTER>*PT. Tirta Investama Klaten*"
			);
			$post_url = 'async_send_message_split';
		}

		$data_string = json_encode($data);
		$ch = curl_init('http://116.203.92.59/api/' . $post_url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_VERBOSE, 0);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 360);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt(
			$ch,
			CURLOPT_HTTPHEADER,
			array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($data_string)
			)
		);
		echo $res = curl_exec($ch);
		curl_close($ch);
        echo "$res";
        exit;
    }

    // public function multiple()
    // {
    //     $x = $this->input->post('receiver');

    //     foreach ($x as $key => $val) {
    //         $data = array(
    //             //'number' => $x[$key],
    //             'number' => $this->datareceiver->get_id(de($val))->row()->phone_number,
    //             'name' => $this->datareceiver->get_id(de($val))->row()->receiver_name,
    //             'message' => $this->input->post('message'),
    //             'image' => $this->input->post('image')
    //         );
    //         $this->datamessage->send($data);
    //     }
    //     echo "Success!";
    //     exit;
    // }

    // public function coba()
    // {
    //     $x = $this->input->post('receiver');
    //     $y = $this->input->post('message');

    //     $no = 1;
    //     foreach ($x as $ky => $val) {
    //         $phone_number = $this->dataload->option('telp')->label; // Sender
    //         $phone_no = $this->datareceiver->get_id(de($val))->row()->phone_number; // Receiver
    //         $receiver_name = $this->datareceiver->get_id(de($val))->row()->receiver_name;
    //         $message = $this->datamessage->get_id(de($y))->row()->content;
    //         $image = base_url() . 'uploads/' . $this->datamessage->get_id(de($y))->row()->image;
    //         $key = $this->dataload->option('generate-key')->label;

    //         $message = preg_replace("/(\n)/", "<ENTER>", $message);
    //         $message = preg_replace("/(\r)/", "<ENTER>", $message);

    //         $phone_no = preg_replace("/(\n)/", ",", $phone_no);
    //         $phone_no = preg_replace("/(\r)/", "", $phone_no);

    //         # Variable dan statement "kirim dengan image" dan "kirim tanpa image"
    //         $img = $this->datamessage->get_id(de($y))->row()->image;
    //         //$data = NULL;            
    //         if (empty($img)) {
    //             $data = array(
    //                 'no' => $no++,
    //                 'phone_no' => $phone_no .' - '. $receiver_name,
    //                 //"key" => "$key",
    //                 //"message" => "Kepada *" . $receiver_name . "*<ENTER>" . $message . "<ENTER>*PT. Tirta Investama Klaten*"
    //             );
    //             print_r($data);
    //         } else {
    //             $data = array(
    //                 'no' => $no++,
    //                 'phone_no' => $phone_no .' - '. $receiver_name,
    //                 //"key" => "$key",
    //                 //"url" => "$image",
    //                 //"message" => "Kepada *" . $receiver_name . "*<ENTER>" . $message . "<ENTER>*PT. Tirta Investama Klaten*"
    //             );
    //             print_r($data);
    //         }  
                               
    //     }
    // }

    // public function whatsapp()
    // {
    //     $x = $this->input->post('receiver');
    //     $y = $this->input->post('message');

    //     foreach ($x as $key => $val) {
    //         $phone_number = $this->dataload->option('telp')->label; // Sender
    //         $phone_no = $this->datareceiver->get_id(de($val))->row()->phone_number; // Receiver
    //         $receiver_name = $this->datareceiver->get_id(de($val))->row()->receiver_name;
    //         $message = $this->datamessage->get_id(de($y))->row()->content;
    //         $image = base_url() . 'uploads/' . $this->datamessage->get_id(de($y))->row()->image;
    //         $key = $this->dataload->option('generate-key')->label;

    //         $message = preg_replace("/(\n)/", "<ENTER>", $message);
    //         $message = preg_replace("/(\r)/", "<ENTER>", $message);

    //         $phone_no = preg_replace("/(\n)/", ",", $phone_no);
    //         $phone_no = preg_replace("/(\r)/", "", $phone_no);

    //         # Variable dan statement "kirim dengan image" dan "kirim tanpa image"
    //         $img = $this->datamessage->get_id(de($y))->row()->image;
    //         if (empty($img)) {
    //             $data = array(
    //                 "phone_no" => $phone_no,
    //                 "key" => "$key",
    //                 "message" => "Kepada *" . $receiver_name . "*<ENTER>" . $message . "<ENTER>*PT. Tirta Investama Klaten*"
    //             );
    //             $post_url = 'async_send_message_split';
    //         } else {
    //             $data = array(
    //                 "phone_no" => $phone_no,
    //                 "key" => "$key",
    //                 "url" => "$image",
    //                 "message" => "Kepada *" . $receiver_name . "*<ENTER>" . $message . "<ENTER>*PT. Tirta Investama Klaten*"
    //             );
    //             $post_url = 'async_send_image_url';
    //         }

    //         $data_string = json_encode($data);
    //         $ch = curl_init('http://116.203.92.59/api/' . $post_url);
    //         curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    //         curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
    //         curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    //         curl_setopt($ch, CURLOPT_VERBOSE, 0);
    //         curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
    //         curl_setopt($ch, CURLOPT_TIMEOUT, 360);
    //         curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    //         curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    //         curl_setopt(
    //             $ch,
    //             CURLOPT_HTTPHEADER,
    //             array(
    //                 'Content-Type: application/json',
    //                 'Content-Length: ' . strlen($data_string)
    //             )
    //         );
    //         echo $res = curl_exec($ch);
    //         curl_close($ch);
    //     }
    //     echo "Success!";
    //     exit;
    // }
}
