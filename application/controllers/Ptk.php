<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ptk extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('login_admin') == FALSE) {
			reject();
		}

		$this->load->model('dataptk');
		$this->load->model('datajenisptk');
		$this->load->helper('url');
		$this->load->library('encrypt');
	}

	public function index()
	{
		$data['title'] = "PTK"; //title
		$data['url'] = base_url('ptk/all'); //url
		$data['jenis_ptk'] = $this->datajenisptk->get_all()->result();

		$this->template->load('layout_dashboard', 'page_dashboard/ptk', $data);
	}

	public function all()
    {
        $record = $this->datajenisptk->get_all()->result();
        $record = $this->dataptk->get_all()->result();
        $no = 1;
        foreach ($record as $d) {
            $tbody = array();
            $tbody[] = $no++;
            $tbody[] = $d->nip;
            $tbody[] = $d->nama;
            $tbody[] = $d->telp;
            $tbody[] = $d->nama_jenis_ptk;
            $tbody[] = $d->email;
            $tbody[] = '<span class="dropdown show">
			<a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true"><i class="la la-ellipsis-h"></i></a>
			<div class="dropdown-menu dropdown-menu-right" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-196px, -24px, 0px);">
				<button data-id=' . en($d->ptk_id) . ' data-toggle="modal" class="edit-data dropdown-item"><i class="la la-edit"></i> Edit</button>
				<button data-id=' . en($d->ptk_id) . ' data-toggle="modal" class="delete-data dropdown-item" ><i class="la la-trash"></i> Hapus</button>
			</div>
			</span>';
            $data[] = $tbody;
        }

        if ($record) {
            echo json_encode(array('data' => $data));
        } else {
            echo json_encode(array('data' => 0));
        }
	}
	
	public function edit()
    {
        $id = de($this->input->post('id'));
        $data['e'] = $this->dataptk->get_id($id)->row();
        $data['jenis_ptk'] = $this->datajenisptk->get_all()->result();

        $this->load->view('page_dashboard/ptk_edit', $data); //layout
    }

	public function create()
	{
        $password = $this->input->post('password');
		$record = array(
			'nip' => $this->input->post('nip'),
			'nama' => $this->input->post('nama'),
			'telp' => $this->input->post('telp'),
			'jenis_ptk_id' => $this->input->post('jenis_ptk_id'),
            'email' => $this->input->post('email'),
            'password' => md5($this->input->post('password')),        
        );
        if(!empty($password)){
            $record['password'] = md5($password);
        }
		$data = $this->dataptk->add($record);
		echo json_encode($data);
    }
    
    public function update()
    {
        $id = de($this->input->post('id'));
        $password = $this->input->post('password');
        $record = array(
            'nip' => $this->input->post('nip'),
			'nama' => $this->input->post('nama'),
			'telp' => $this->input->post('telp'),
			'jenis_ptk_id' => $this->input->post('jenis_ptk_id'),
            'email' => $this->input->post('email'),
        );
        if ($password !== "") {
            $record['password'] = md5($password);
        }
        $where = array(
            'ptk_id' => $id
        );

        $data = $this->dataptk->update($where, $record);
        echo json_encode($data);
    }

	public function delete()
    {
        $id = de($this->input->post('id'));
        $where = array(
            'ptk_id' => $id
        );
        $data = $this->dataptk->delete($where);
        echo json_encode($data);
    }
}