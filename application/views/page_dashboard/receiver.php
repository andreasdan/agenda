<div class="m-content">
    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30"
        role="alert">
        <div class="m-alert__icon">
            <i class="flaticon-questions-circular-button m--font-brand"></i>
        </div>
        <div class="m-alert__text">
            Halaman ini digunakan untuk menambahkan, menyunting dan menghapus data penerima pesan.
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Daftar <?php echo $title ?>
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <div>
                <div class="mb-4">
                    <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-backdrop="static"
                        data-target="#exampleModalRight">+ Tambah Data</button>
                    <button type="button" id="delete" class="btn btn-outline-danger">- Hapus Data</button>
                </div>
                <br><br><br>
                <div class="table-responsive">
                    <table class="data_table table table-striped- table-bordered table-hover table-checkable">
                        <thead>
                            <tr>
                                <th><input type="checkbox" id="check_all" value='1'></th>
                                <th>No</th>
                                <th>NIK</th>
                                <th>Nama</th>
                                <th>Telp</th>
                                <th>Bagian</th>
                                <th>Pilihan</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
								$no = 1;
								foreach ($data as $d) :
								?>
                            <tr id='tr_<?php echo $d->receiver_id ?>'>
                                <td align=' center'><input type="checkbox" class='checkbox' name='delete[]'
                                        value='<?php echo $d->receiver_id ?>'></td>
                                <td width="10%"><?php echo $no++ ?></td>
                                <td><?php echo $d->id_number ?></td>
                                <td><?php echo $d->receiver_name ?></td>
                                <td><?php echo $d->phone_number ?></td>
                                <td><?php echo department($d->department) ?></td>
                                <td width="20%" align="center">
                                    <button class="edit-data btn btn-outline-primary btn-xs mb-1"
                                        data-id="<?php echo en($d->receiver_id)?>">Edit</button>
                                    <button class="delete-data btn btn-outline-danger btn-xs mb-1"
                                        data-id="<?php echo en($d->receiver_id)?>">Hapus</button>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
$(document).ready(function() {
    var table = $('.data_table').DataTable();
    //table.columns(5).search("SF").draw();
});
</script>

<!-- Modal Input-->
<div class="modal fade bd-example-modal-lg" id="exampleModalRight" tabindex="-1" role="dialog"
    aria-labelledby="exampleModalRight" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Input <?php echo $title ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form name="add_name" id="add_name">
                <div id="dynamicContent">
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Nomor Whatsapp</label>
                            <table id="dynamic_field">
                                <tr>
                                    <td>
                                        <div class="input-group mb-2 mr-sm-2">
                                            <input type="text" name="id_number[]" placeholder="NIK" required
                                                class="form-control">
                                            &nbsp; &nbsp;
                                            <input type="text" name="receiver_name[]" placeholder="Nama" required
                                                class="form-control">
                                            &nbsp; &nbsp;
                                            <div class="input-group-prepend">
                                                <div class="input-group-text">+62</div>
                                            </div>
                                            <input type="text" name="phone_number[]" placeholder="Nomor HP" required
                                                class="form-control">
                                            &nbsp; &nbsp;
                                            <select class="mySelect form-control" name="department[]" required>
                                                <option value="">- Departement -</option>
                                                <option value="1"><?php echo department(1) ?></option>
                                                <option value="2"><?php echo department(2) ?></option>
                                            </select>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <button type="button" name="add" id="add" class="btn btn-outline-primary">+ Tambah
                            Field</button>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-outline-primary" data-dismiss="modal">Batal</button>
                        <button type="button" id="submit" name="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Edit -->
<div class="modal fade bd-example-modal-lg" id="modal_form_edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit <?php echo $title ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="page_edit">
            </div>
        </div>
    </div>
</div>

<!-- Ajax add number -->
<script>
$(document).ready(function() {
    var i = 1;
    $('#add').click(function() {
        i++;
        $('#dynamic_field').append('<tr id="row' + i +
            '"><td><div class="input-group mb-2 mr-sm-2"><input type="text" name="id_number[]" placeholder="NIK" required class="form-control">&nbsp; &nbsp;<input type="text" name="receiver_name[]" placeholder="Nama" required class="form-control">&nbsp; &nbsp;<div class="input-group-prepend"><div class="input-group-text">+62</div></div><input type="text" name="phone_number[]" placeholder="Nomor HP" required autofocus class="form-control">&nbsp; &nbsp;<input type="text" name="grade[]" placeholder="Isikan Grade" class="form-control">&nbsp; &nbsp;<select class="mySelect form-control" name="department[]" required><option value="">- Departement -</option><option value="1"><?php echo department(1) ?></option><option value="2"><?php echo department(2) ?></option></select></div></td><td><i name="remove" id="' +
            i + '" class="simple-icon-close red btn_remove"></i></td></tr>');
    });

    $(document).on('click', '.btn_remove', function() {
        var button_id = $(this).attr("id");
        $('#row' + button_id + '').remove();
    });

    $('#submit').click(function() {
        $.ajax({
            url: "<?php echo base_url() ?>receiver/create",
            method: "POST",
            data: $('#add_name').serialize(),
            success: function() {
                location.reload();
                // Toast sukses
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })
                Toast.fire({
                    icon: 'success',
                    title: 'Menambahkan <?php echo $title ?>'
                })
                $('#exampleModalRight').modal('hide');
                //location.reload();
            }
        });
    });

});
</script>

<!-- Delete Checkbox -->
<script type="text/javascript">
$(document).ready(function() {

    // Check all
    $("#check_all").change(function() {

        var checked = $(this).is(':checked');
        if (checked) {
            $(".checkbox").each(function() {
                $(this).prop("checked", true);
            });
        } else {
            $(".checkbox").each(function() {
                $(this).prop("checked", false);
            });
        }
    });

    // Changing state of check_all checkbox 
    $(".checkbox").click(function() {

        if ($(".checkbox").length == $(".checkbox:checked").length) {
            $("#check_all").prop("checked", true);
        } else {
            $("#check_all").prop("checked", false);
        }

    });

    // Edit
    $('.data_table').on('click', '.edit-data', function() {
        $.ajax({
            type: "post",
            url: "<?php echo base_url('receiver/edit') ?>",
            data: {
                id: $(this).data('id')
            },
            success: function(data) {
                swal.close();
                $('#modal_form_edit').modal('show');
                $('#page_edit').html(data);

                // proses untuk mengubah data
                $('#form_edit').on('submit', function() {

                    $.ajax({
                        url: "<?php echo base_url('receiver/update') ?>",
                        type: "POST",
                        data: new FormData($('#form_edit')[0]),
                        processData: false,
                        contentType: false,
                        success: function(data) {
                            //mydata.ajax.reload(null, false);
                            // Toast sukses
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                onOpen: (toast) => {
                                    toast.addEventListener(
                                        'mouseenter',
                                        Swal.stopTimer)
                                    toast.addEventListener(
                                        'mouseleave',
                                        Swal.resumeTimer
                                    )
                                }
                            })
                            Toast.fire({
                                type: 'success',
                                title: 'Menyunting <?php echo $title ?>'
                            })
                            // bersihkan form pada modal
                            $('#modal_form_edit').modal('hide');
                            location.reload();
                        }
                    })
                    return false;
                });
            }
        });
    });

    // Delete button clicked
    $('#delete').click(function() {
        // Confirm alert
        Swal.fire({
            title: 'Hapus',
            text: 'Apakah Anda yakin akan menghapus data ini?',
            icon: "question",
            confirmButtonColor: '#d9534f',
            showCancelButton: true,
        }).then((result) => {
            if (result.value) {
                // Get receiversid from checked checkboxes
                var receivers_arr = [];
                $(".checkbox:checked").each(function() {
                    var receiversid = $(this).val();

                    receivers_arr.push(receiversid);
                });

                // Array length
                var length = receivers_arr.length;

                if (length > 0) {

                    // AJAX request
                    $.ajax({
                        url: '<?php echo base_url() ?>receiver/delete_multiple',
                        type: 'post',
                        data: {
                            receivers_ids: receivers_arr
                        },
                        success: function(response) {

                            // Remove <tr>
                            $(".checkbox:checked").each(function() {
                                var receiversid = $(this).val();

                                $('#tr_' + receiversid).remove();
                            });
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                onOpen: (toast) => {
                                    toast.addEventListener('mouseenter',
                                        Swal.stopTimer)
                                    toast.addEventListener('mouseleave',
                                        Swal.resumeTimer)
                                }
                            })
                            Toast.fire({
                                icon: 'success',
                                title: 'Menghapus <?php echo $title ?>'
                            })
                        }
                    });
                }
            } else if (result.dismiss === swal.DismissReason.cancel) {
                // Toast sukses
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })
                Toast.fire({
                    icon: 'error',
                    title: 'Batal menghapus <?php echo $title ?>'
                })
            }
        })

    });

    // Delete Data satuan
    $('.data_table').on('click', '.delete-data', function() {
        Swal.fire({
            title: 'Konfirmasi',
            text: "Anda yakin menghapus data <?php echo $title ?>?",
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Ya, Hapus',
            confirmButtonColor: '#ff5e5e',
            cancelButtonColor: '#3085d6',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "<?php echo base_url('receiver/delete') ?>",
                    method: "post",
                    data: {
                        id: $(this).data('id')
                    },
                    success: function(data) {
                        // Toast sukses
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000,
                            timerProgressBar: true,
                            onOpen: (toast) => {
                                toast.addEventListener('mouseenter',
                                    Swal.stopTimer)
                                toast.addEventListener('mouseleave',
                                    Swal.resumeTimer)
                            }
                        })
                        Toast.fire({
                            type: 'success',
                            title: 'Menghapus <?php echo $title ?>'
                        })
                        //mydata.ajax.reload(null, false)
                        location.reload();
                    }
                })
            } else if (result.dismiss === swal.DismissReason.cancel) {
                // Toast sukses
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })
                Toast.fire({
                    type: 'error',
                    title: 'Batal menghapus <?php echo $title ?>'
                })
            }
        })
    });

});
</script>