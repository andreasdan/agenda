<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jenisptk extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('login_admin') == FALSE) {
			reject();
		}

		$this->load->model('datajenisptk');
		$this->load->library('encryption');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
	}

	public function index()
	{
		$data['title'] = "Jenis PTK"; //title
		$data['url'] = base_url('jenisptk/all'); //url

		$this->template->load('layout_dashboard', 'page_dashboard/jenisptk', $data);
	}

	public function all()
    {
        $record = $this->datajenisptk->get_all()->result();
        $no = 1;
        foreach ($record as $d) {
            $tbody = array();
            $tbody[] = $no++;
            $tbody[] = $d->nama_jenis_ptk;
            $tbody[] = '<span class="dropdown show">
			<a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true"><i class="la la-ellipsis-h"></i></a>
			<div class="dropdown-menu dropdown-menu-right" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-196px, -24px, 0px);">
				<button data-id=' . en($d->jenis_ptk_id) . ' data-toggle="modal" class="edit-data dropdown-item"><i class="la la-edit"></i> Edit</button>
				<button data-id=' . en($d->jenis_ptk_id) . ' data-toggle="modal" class="delete-data dropdown-item" ><i class="la la-trash"></i> Hapus</button>
			</div>
			</span>';
            $data[] = $tbody;
        }

        if ($record) {
            echo json_encode(array('data' => $data));
        } else {
            echo json_encode(array('data' => 0));
        }
	}
	
	public function edit()
    {
        $id = de($this->input->post('id'));
        $data['e'] = $this->datajenisptk->get_id($id)->row();

        $this->load->view('page_dashboard/jenisptk_edit', $data); //layout
    }

	public function create()
	{
		$record = array(
			'nama_jenis_ptk' => $this->input->post('nama'),
		);
		$data = $this->datajenisptk->add($record);
		echo json_encode($data);
    }
    
    public function update()
    {
        $id = de($this->input->post('id'));
        $record = array(
            'nama_jenis_ptk' => $this->input->post('nama'),
        );
        $where = array(
            'jenis_ptk_id' => $id
        );

        $data = $this->datajenisptk->update($where, $record);
        echo json_encode($data);
    }

	public function delete()
    {
        $id = de($this->input->post('id'));
        $where = array(
            'jenis_ptk_id' => $id
        );
        $data = $this->datajenisptk->delete($where);
        echo json_encode($data);
    }
}