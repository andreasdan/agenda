<form id="form_edit" name="form2" method="POST" enctype="multipart/form-data">
    <div class="modal-body">
        <input type="hidden" name="id" value="<?php echo en($e->message_id) ?>" type="text"
            class="form-control input-default" required>
        <div id="dynamicContent2">
            <div class="modal-body">
                <div class="form-group">
                    <label>Preview:</label>
                    <div id="preview2" class="unselectable">
                        <font color="silver">Text Simulation</font>
                    </div>
                    </td>
                </div>
                <br><br><br>
                <div class="form-group">
                    <div class="btn-group btn-group-sm  mr-2  mb-1" role="group">
                        <button type="button" onclick="javascript: formatf(0); this.blur();" name="buttonBold2"
                            id="buttonBold2" class="btn btn-secondary" data-toggle="tooltip" data-placement="top"
                            title="Bold"><b>B</b></button>
                        <button type="button" onclick="javascript: formatf(1); this.blur();" disabled="disabled"
                            name="buttonItalic2" id="buttonItalic2" class="btn btn-secondary" data-toggle="tooltip"
                            data-placement="top" title="Italic"><i>I</i></button>
                        <button type="button" onclick="javascript: formatf(2); this.blur();" disabled="disabled"
                            name="buttonStrikethrough2" id="buttonStrikethrough2" class="btn btn-secondary"
                            data-toggle="tooltip" data-placement="top" title="Strikethrough"><s>S</s></button>
                        <button type="button" onclick="javascript: formatf(3); this.blur();" disabled="disabled"
                            name="buttonMonospace2" id="buttonMonospace2" class="btn btn-secondary"
                            data-toggle="tooltip" data-placement="top" title="Monospace"><tt>M</tt></button>
                    </div>
                </div>
                <input type="text" name="_key" value="<?php echo en($e->message_id) ?>" hidden required>
                <div class="form-group">
                    <label>Isi Pesan</label>
                    <textarea name="whatsappText" id="whatsappText2" class="form-control"
                        onkeyup="javascript: updatef();" onmouseup="javascript: updatef();"
                        onselect="javascript: whatsappText2Selected();"
                        placeholder="WhatsApp Message Text"><?php echo $e->content ?></textarea>
                </div>
                <div class="form-group">
                    <label>Keterangan</label>
                    <input type="text" name="memo" value="<?php echo $e->memo ?>" class="form-control">
                </div>
                <div class="form-group">
                    <label class="text-danger">
                        - Pilih <code>Hapus Gambar</code> apabila ingin menghapus gambar sebelumnya. Otomatis pesan tanpa gambar.<br>
                        - Pilih <code>Ganti Gambar</code> apabila ingin mengubah gambar.
                    </label>
                </div>
                <div class="form-group">
                    <input type="checkbox" name="image_remove" value="yes"> Hapus Gambar
                </div>
                <div class="form-group">
                    <label>Ganti Gambar</label>
                    <input type="file" name="file_image">
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="reset" class="btn btn-outline-danger" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success">Simpan</button>
    </div>
</form>