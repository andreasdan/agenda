<div class="m-content">
    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30"
        role="alert">
        <div class="m-alert__icon">
            <i class="flaticon-questions-circular-button m--font-brand"></i>
        </div>
        <div class="m-alert__text">
            Halaman ini digunakan untuk mengolah halaman <?php echo $title ?> meliputi membuat <?php echo $title ?>
            baru, edit <?php echo $title ?> dan menonaktifkan <?php echo $title ?>.
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Daftar <?php echo $title ?>
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="table-responsive">

                <!--begin: Datatable -->
                <table class="data_table table table-striped- table-bordered table-hover table-checkable" id="">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Tanggal Kirim</th>
                            <th>Nama PTK</th>
                            <th>Jenis PTK</th>
                            <th>Nama Agenda</th>
                            <th>Tanggal</th>
                            <th>Jam</th>
                            <th>Tempat</th>
                            <th>Keterangan</th>
                            <th width="10%">Pilihan</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>

<script type="text/javascript">
$(document).ready(function() {
    // ini adalah fungsi untuk mengambil data ke dalam datatable
    var mydata = $('.data_table').DataTable({
        "processing": true,
        "ajax": "<?php echo $url ?>",
        stateSave: true,
        "searching": true,
        "order": []
    })
    // fungsi untuk menambah data
    $('#form_input').on('submit', function() {
        $.ajax({
            url: "<?php echo base_url('kirim/create') ?>",
            type: "POST",
            data: new FormData($('#form_input')[0]),
            processData: false,
            contentType: false,
            success: function(data) {
                mydata.ajax.reload(null, false);
                // Toast sukses
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })
                Toast.fire({
                    type: 'success',
                    title: 'Menambahkan <?php echo $title ?>'
                })
                // bersihkan form pada modal
                $('#modal_form_input').modal('hide');
                $("#form_input")[0].reset();

            }
        })
        return false;
    });
    // fungsi untuk hapus data
    //pilih selector dari table id dengan class .delete-data
    $('.data_table').on('click', '.delete-data', function() {
        Swal.fire({
            title: 'Konfirmasi',
            text: "Anda yakin menghapus data <?php echo $title ?>?",
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Ya, Hapus',
            confirmButtonColor: '#ff5e5e',
            cancelButtonColor: '#3085d6',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "<?php echo base_url('kirim/delete') ?>",
                    method: "post",
                    data: {
                        id: $(this).data('id')
                    },
                    success: function(data) {
                        // Toast sukses
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000,
                            timerProgressBar: true,
                            onOpen: (toast) => {
                                toast.addEventListener('mouseenter',
                                    Swal.stopTimer)
                                toast.addEventListener('mouseleave',
                                    Swal.resumeTimer)
                            }
                        })
                        Toast.fire({
                            type: 'success',
                            title: 'Menghapus <?php echo $title ?>'
                        })
                        mydata.ajax.reload(null, false)
                    }
                })
            } else if (result.dismiss === swal.DismissReason.cancel) {
                // Toast sukses
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })
                Toast.fire({
                    type: 'error',
                    title: 'Batal menghapus <?php echo $title ?>'
                })
            }
        })
    });

});
</script>