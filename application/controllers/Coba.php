<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Coba extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('url');
	}

	public function index()
	{	
        $x = '709006800';
        $a = ltrim($x, '0');
        echo $a;
    }
    
    public function text()
	{	
		$data['title'] = "Penerima Pesan"; //content page
		//$data['data'] = $this->datauser->daftar()->result();
		
		$data['page'] = "coba"; //content page
		$this->load->view('layout_dashboard',$data); //layout
	}

	public function test()
    {
		$phone_no = '6282136177322'; // Receiver
		$message = 'Andreas Dan Testing';
		$img = 'https://automatika.id/assets/img/automatika-black.png';
		$key = '146913a17eba125587d201a30291fc0fc4307af815ddedfc';
		

		$message = preg_replace("/(\n)/", "<ENTER>", $message);
		$message = preg_replace("/(\r)/", "<ENTER>", $message);

		$phone_no = preg_replace("/(\n)/", ",", $phone_no);
		$phone_no = preg_replace("/(\r)/", "", $phone_no);
		
		if (empty($img)) {
			$data = array(
				"phone_no" => $phone_no,
				"key" => "$key",
				"message" => $message . "<ENTER>*PT. Tirta Investama Klaten*"
			);
			$post_url = 'async_send_message_split';
		} else {
			$data = array(
				"phone_no" => $phone_no,
				"key" => "$key",
				"url" => "$img",
				"message" => $message . "<ENTER>*PT. Tirta Investama Klaten*"
			);
			$post_url = 'async_send_image_url';
		}

		$data_string = json_encode($data);
		$ch = curl_init('http://116.203.92.59/api/' . $post_url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_VERBOSE, 0);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 360);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt(
			$ch,
			CURLOPT_HTTPHEADER,
			array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($data_string)
			)
		);
		$res = curl_exec($ch);
		curl_close($ch);
        echo json_encode($res);
        exit;
	}
	
	public function lihat()
    {
		$phone_no = '6282136177322'; // Receiver
		$key = '146913a17eba125587d201a30291fc0fc4307af815ddedfc';
		
		$data = array(
			//"phone_no" => $phone_no,
			"key" => "$key",
			"time_choose" => "yesterday"
		);
		$post_url = 'report_json';

		$data_string = json_encode($data);
		$ch = curl_init('http://116.203.92.59/api/' . $post_url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_VERBOSE, 0);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 360);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt(
			$ch,
			CURLOPT_HTTPHEADER,
			array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($data_string)
			)
		);
		$res = curl_exec($ch);
		curl_close($ch);
		exit;
        echo $res;
        
	}
	
	public function qr()
    {
		$key = '146913a17eba125587d201a30291fc0fc4307af815ddedfc';
		
		$data = array(
			"key" => "$key",
		);
		$post_url = 'generate_qr';

		$data_string = json_encode($data);
		$ch = curl_init('http://116.203.92.59/api/' . $post_url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_VERBOSE, 0);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
		curl_setopt($ch, CURLOPT_TIMEOUT, 360);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt(
			$ch,
			CURLOPT_HTTPHEADER,
			array(
				'Content-Type: application/json',
				'Content-Length: ' . strlen($data_string)
			)
		);
		$res = curl_exec($ch);
		curl_close($ch);
		echo $res;
		exit;        
	}
	
	public function olee()
    {
		echo 'test';
    }
}