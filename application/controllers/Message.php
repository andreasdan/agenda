<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Message extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('level') !== '1') {
			reject();
		}

		$this->load->model('datamessage');
		$this->load->model('datareceiver');
		$this->load->library('encryption');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
	}

	public function index()
	{
		$data['title'] = "Pesan"; //title
		$data['url'] = base_url('message/all'); //url

		$this->template->load('layout_dashboard', 'page_dashboard/message', $data);
	}

	public function all()
    {
        $record = $this->datamessage->get_all()->result();
        $no = 1;
        foreach ($record as $d) {
            if (empty($d->image) || empty(file_exists('uploads/' . $d->image))) {
                $image = '';
            } else {
                $image = '<img class="mr-3 circle-rounded" width="50" height="50" src="' . base_url() . 'uploads/' . $d->image . '" alt=" ">';
            }
            $tbody = array();
            $tbody[] = $no++;
            $tbody[] = $image;
            $tbody[] = $d->content;
            $tbody[] = $d->memo;
            $tbody[] = '<span class="dropdown show">
			<a href="#" class="btn m-btn m-btn--hover-brand m-btn--icon m-btn--icon-only m-btn--pill" data-toggle="dropdown" aria-expanded="true"><i class="la la-ellipsis-h"></i></a>
			<div class="dropdown-menu dropdown-menu-right" x-placement="top-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-196px, -24px, 0px);">
				<button data-id=' . en($d->message_id) . ' data-toggle="modal" class="edit-data dropdown-item"><i class="la la-edit"></i> Edit</button>
				<a href="'.base_url('select/all/' . en($d->message_id)).'" class="dropdown-item"><i class="la la-send"></i> Kirim</a>
				<button data-id=' . en($d->message_id) . ' data-toggle="modal" class="delete-data dropdown-item" ><i class="la la-trash"></i> Hapus</button>
			</div>
			</span>';
            $data[] = $tbody;
        }

        if ($record) {
            echo json_encode(array('data' => $data));
        } else {
            echo json_encode(array('data' => 0));
        }
	}
	
	public function edit()
    {
        $id = de($this->input->post('id'));
        $data['e'] = $this->datamessage->get_id($id)->row();

        $this->load->view('page_dashboard/message_edit', $data); //layout
    }

	public function detail($encrypt_id)
	{
		$id = de($encrypt_id);
		$data['title'] = "Pesan"; //title
		$data['data'] = $this->datamessage->get_id($id)->result();

		$data['page'] = "message_detail"; //content page
		$this->load->view('layout_dashboard', $data); //layout
	}

	public function send($encrypt_id)
	{
		$id = de($encrypt_id);
		$data['title'] = "Pesan"; //title
		$data['data'] = $this->datamessage->get_id($id)->result();
		$data['receiver'] = $this->datareceiver->get_all()->result();

		$data['page'] = "message_send"; //content page
		$this->load->view('layout_dashboard', $data); //layout
	}

	public function select($encrypt_id)
	{
		$id = de($encrypt_id);
		$data['title'] = "Pesan"; //title
		$data['data'] = $this->datamessage->get_id($id)->result();
		$data['all'] = $this->datareceiver->get_all()->result();
		//Department
		$data['1'] = $this->datareceiver->get_department(1)->result();

		$this->template->load('layout_dashboard', 'page_dashboard/message_select', $data);
	}

	public function create()
	{
		// memanggil private function untuk upload gambar
		$fileimage = $this->upload_image();

		// mendapatkan nama file foto
		$get_image = $fileimage['file_name'];

		$record = array(
			'content' 	=> $this->input->post('whatsappText'),
			'image' 	=> $get_image,
			'memo' => $this->input->post('memo')
		);
		$this->datamessage->insert($record);
		$this->session->set_flashdata('insert_success', 'Success!');
		redirect('message');
	}

	public function update()
	{

		$encrypt_id = $this->input->post('_key');
		$id = de($encrypt_id);
		$image_remove = $this->input->post('image_remove');
		$file = $this->input->post('file_image');
		$old_image = $this->datamessage->get_id($id)->row()->image;
		$record = array(
			'content' => $this->input->post('whatsappText'),
			'memo' => $this->input->post('memo')
		);

		# Hapus Foto
		if ($image_remove == 'yes'){
            if (file_exists('uploads/' . $old_image)) {
				unlink('uploads/' . $old_image);
            }
		}

		# Ganti Foto
		// memanggil private function untuk upload gambar
		$fileimage = $this->upload_image();
		// mendapatkan nama file foto
		$get_image = $fileimage['file_name'];

		// delete old image;
		if (!empty($get_image)) {
			$record['image'] = $get_image; // insert image to table
			if (file_exists('uploads/' . $old_image)) {
				unlink('uploads/' . $old_image);
			}
		}

		$where = array(
			'message_id' => $id
		);

		$this->datamessage->update($where, $record);
		
		echo 'berhasil';
	}

	public function update_image()
	{
		$encrypt_id = $this->input->post('_key');
		$id = de($encrypt_id);

		// memanggil private function untuk upload gambar
		$fileimage = $this->upload_image();

		// mendapatkan nama file foto
		$get_image = $fileimage['file_name'];

		// delete old image
		$old_image = $this->input->post('old_image');
		if (file_exists('uploads/' . $old_image) && $old_image)
			unlink('uploads/' . $old_image);

		$record = array(
			'image' => $get_image
		);
		$where = array(
			'message_id' => $id
		);

		$this->datamessage->update($where, $record);
		$this->session->set_flashdata('update_success', 'Success!');
		redirect('message');
	}

	public function delete()
    {
        $id = de($this->input->post('id'));
        $where = array(
            'message_id' => $id
        );
        $data = $this->datamessage->delete($where);
        echo json_encode($data);
    }

	//Private
	private function upload_image()
	{
		// Setup folder upload path
		$config['upload_path']		= './uploads/';

		// Setup file yang di izinkan
		$config['allowed_types']	= 'gif|jpg|png|jpeg';

		// Encrpt nama foto agar tidak sama
		$config['encrypt_name']		= TRUE;

		// Memanggil library upload disertai dengan paramter $config array
		$this->load->library('upload', $config);

		// jika upload gagal, return false
		if ($this->upload->do_upload('file_image') == false) {
			return false;
			#return $this->upload->display_errors();
		}

		// jika upload berhasil, return nama file dan membuat thumbnail foto
		else {
			// Mengambil data yang berhasil di upload
			$uploaded_data = $this->upload->data();

			// Mendapatkan nama file
			$file_name = $uploaded_data['file_name'];

			// Memanggil library GD 2
			$config['image_library'] = 'gd2';

			// Memanggil nama file images
			$config['source_image'] = './uploads/' . $file_name;

			// Membuat thumbnail
			$config['create_thumb'] = FALSE;

			// Mempertahankan foto berdasarkan ratio, hal ini digunakan agar foto tidak gepeng
			$config['maintain_ratio'] = TRUE;

			// Setting lebar dan tinggi
			//$config['width']         = 100;
			//$config['height']       = 100;

			// Memanggil library image_lib untuk memproses images resize disertai dengan parameter $config
			$this->load->library('image_lib', $config);

			// Melakukan resize
			$this->image_lib->resize();

			// Return data
			return $uploaded_data;
		}
	}
}