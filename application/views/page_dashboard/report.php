<div class="m-content">
    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30"
        role="alert">
        <div class="m-alert__icon">
            <i class="flaticon-questions-circular-button m--font-brand"></i>
        </div>
        <div class="m-alert__text">
            Halaman ini digunakan untuk mengolah halaman <?php echo $title ?> meliputi membuat <?php echo $title ?>
            baru, edit <?php echo $title ?> dan menonaktifkan <?php echo $title ?>.
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Daftar <?php echo $title ?>
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <!-- <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <button type="button"
                            class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air"
                            data-toggle="modal" data-target="#modal_form_input">
                            <span>
                                <i class="la la-plus"></i>
                                <span>Laporan</span>
                            </span>
                        </button>
                    </li>
                </ul> -->
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="table-responsive">

                <!--begin: Datatable -->
                <table class="data_table table table-striped- table-bordered table-hover table-checkable" id="">
                    <thead>
                        <tr>
                            <th>Telp</th>
                            <th>Pesan</th>
                            <th>Tgl</th>
                            <th>Status</th>
                            <th>Keterangan</th>
                        </tr>
                    </thead>
                    <tbody id="list_table">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>

<!-- Modal Create -->
<div class="modal fade bd-example-modal-lg" id="modal_form_input" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form name="form" id="form_input" method="POST" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title">Pencarian Laporan</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="dynamicContent">
                        <div class="modal-body">
                            <div class="form-group">
                                <label>Periode</label>
                                <select type="text" name="time" class="form-control">
                                    <option value='today'>Hari ini</option>
                                    <option value='yesterday'>Kemarin</option>
                                </select>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-danger" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Tampilkan</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Edit -->
<div class="modal fade bd-example-modal-lg" id="modal_form_edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit <?php echo $title ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="page_edit">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {

    $('.data_table').DataTable({
        "processing": true,
        //"serverSide": true,
        "searching": true,
        "stateSave": true,
        "order": [],
        "ajax": {
            "url": "<?php echo $url ?>",
            "type": "POST"
        },
        "dom": 'Bfrtip',
        "buttons": [{
            extend: 'excelHtml5',
            title: '<?php echo $title ?>',
            text: '<button class="btn btn-success mr-auto"><i class="fas fa-file-excel"></i> Export ke excel</button>',
        }],
        "columns": [
            { "data": "penerima" },
            { "data": "isi_pesan" },
            { "data": "tanggal" },
            { "data": "status" },
            { "data": "message_type" }
        ]
    })

    // $('#form_input').on('submit', function() {
    //     $.ajax({
    //         url: "<?php echo base_url('report.json') ?>",
    //         type: "POST",
    //         data: new FormData($('#form_input')[0]),
    //         processData: false,
    //         contentType: false,
    //         cache:false,
    //         success: function(data){
    //             var event_data = '';
    //             $.each(data.data, function(index, value){
    //                 event_data += '<tr>';
    //                 event_data += '<td>'+value.penerima+'</td>';
    //                 event_data += '<td>'+value.isi_pesan+'</td>';
    //                 event_data += '<td>'+value.tanggal+'</td>';
    //                 event_data += '<td>'+value.status+'</td>';
    //                 event_data += '<td>'+value.message_type+'</td>';
    //                 event_data += '</tr>';
    //             });
    //             $(".data_table").append(event_data);
    //             $('#modal_form_input').modal('hide');
    //             $("#form_input")[0].reset();
    //         },
    //         error: function(d){
    //             alert("404. Please wait until the File is Loaded.");
    //         }
    //     })
    //     return false;
    // });

});
</script>