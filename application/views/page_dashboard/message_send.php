<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/loadingio/ldbutton@v1.0.1/dist/ldbtn.min.css" />
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
<!-- <script type="text/javascript" src="<?php echo base_url() ?>assets/js/vendor/cookies/jquery.cookie.min.js"></script> -->
<!-- Radio Button Show Hide -->
<script>
    $(document).ready(function() {
        $(".click-a").click(function() {
            $("#content_a").show();
            $("#content_b").hide();
        });
        $(".click-b").click(function() {
            $("#content_b").show();
            $("#content_a").hide();
        });
    });
</script>

<?php foreach ($data as $d) : ?>
    <main>
        <div class="container-fluid" id="load-contents">
            <div class="row">
                <div class="col-12">
                    <h1>Kirim <?php echo $title ?></h1>
                    <div class="separator mb-5"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card mb-4">
                        <div class="card-body">
                            <button type="button" class="click-a btn btn-outline-primary">Berdasarkan Departemen</button>
                            <button type="button" class="click-b btn btn-outline-primary">Berdasarkan Personal</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="content_a" style="display:none;">
                <div class="col-6">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h3>Kirim Text dan Gambar</h3>
                            <div class="separator mb-5"></div>
                            <form method="post" action="<?php echo base_url('send/department_image') ?>">
                                <div class="form-group">
                                    <select class="mySelect form-control" name="department" required>
                                        <option value="">- Pilih Unit/Departement -</option>
                                        <option value="1"><?php echo department(1) ?> ( Jumlah: <?php echo $this->dataload->sum_department('1'); ?> )</option>
                                        <option value="2"><?php echo department(2) ?> ( Jumlah: <?php echo $this->dataload->sum_department('2'); ?> )</option>
                                        <option value="3"><?php echo department(3) ?> ( Jumlah: <?php echo $this->dataload->sum_department('3'); ?> )</option>
                                        <option value="4"><?php echo department(4) ?> ( Jumlah: <?php echo $this->dataload->sum_department('4'); ?> )</option>
                                        <option value="5"><?php echo department(5) ?> ( Jumlah: <?php echo $this->dataload->sum_department('5'); ?> )</option>
                                        <option value="6"><?php echo department(6) ?> ( Jumlah: <?php echo $this->dataload->sum_department('6'); ?> )</option>
                                        <option value="7"><?php echo department(7) ?> ( Jumlah: <?php echo $this->dataload->sum_department('7'); ?> )</option>
                                        <option value="8"><?php echo department(8) ?> ( Jumlah: <?php echo $this->dataload->sum_department('8'); ?> )</option>
                                        <option value="9"><?php echo department(9) ?> ( Jumlah: <?php echo $this->dataload->sum_department('9'); ?> )</option>
                                    </select>
                                </div>
                                <div class="input-group mb-2 mr-sm-2">
                                    <input type="text" name="_key" value="<?php echo en($d->message_id) ?>" hidden readonly required>
                                    <input type="text" id="a" class="form-control col-2" value="29" oninput="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" hidden readonly required>
                                    <input type="text" id="b" onblur="calculate()" class="form-control col-2" name="x" value="" oninput="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><code>sampai</code></div>
                                    </div>
                                    <input type="text" id="c" class="form-control col-2" name="y" value="" oninput="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                    <div class="input-group-prepend">
                                        <button type="submit" class="btn btn-success ld-ext-right" onclick="this.classList.toggle('running')"><i class="simple-icon-paper-plane"></i> Kirim <img src="<?php echo base_url() ?>assets/img/mini-loading.gif" class="ld"> </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h3>Kirim Text Saja</h3>
                            <div class="separator mb-5"></div>
                            <form method="post" action="<?php echo base_url('send/department_text') ?>">
                                <div class="form-group">
                                    <select class="mySelect form-control" name="department" required>
                                        <option value="">- Pilih Unit/Departement -</option>
                                        <option value="1"><?php echo department(1) ?> ( Jumlah: <?php echo $this->dataload->sum_department('1'); ?> )</option>
                                        <option value="2"><?php echo department(2) ?> ( Jumlah: <?php echo $this->dataload->sum_department('2'); ?> )</option>
                                        <option value="3"><?php echo department(3) ?> ( Jumlah: <?php echo $this->dataload->sum_department('3'); ?> )</option>
                                        <option value="4"><?php echo department(4) ?> ( Jumlah: <?php echo $this->dataload->sum_department('4'); ?> )</option>
                                        <option value="5"><?php echo department(5) ?> ( Jumlah: <?php echo $this->dataload->sum_department('5'); ?> )</option>
                                        <option value="6"><?php echo department(6) ?> ( Jumlah: <?php echo $this->dataload->sum_department('6'); ?> )</option>
                                        <option value="7"><?php echo department(7) ?> ( Jumlah: <?php echo $this->dataload->sum_department('7'); ?> )</option>
                                        <option value="8"><?php echo department(8) ?> ( Jumlah: <?php echo $this->dataload->sum_department('8'); ?> )</option>
                                        <option value="9"><?php echo department(9) ?> ( Jumlah: <?php echo $this->dataload->sum_department('9'); ?> )</option>
                                    </select>
                                </div>
                                <div class="input-group mb-2 mr-sm-2">
                                    <input type="text" name="_key" value="<?php echo en($d->message_id) ?>" hidden readonly required>
                                    <input type="text" id="d" class="form-control col-2" value="29" oninput="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" hidden readonly required>
                                    <input type="text" id="e" onblur="calculate()" class="form-control col-2" name="x" value="" oninput="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text"><code>sampai</code></div>
                                    </div>
                                    <input type="text" id="f" class="form-control col-2" name="y" value="" oninput="this.value=this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
                                    <div class="input-group-prepend">
                                        <button type="submit" class="btn btn-success ld-ext-right" onclick="this.classList.toggle('running')"><i class="simple-icon-paper-plane"></i> Kirim <img src="<?php echo base_url() ?>assets/img/mini-loading.gif" class="ld"></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row" id="content_b" style="display:none;">
                <div class="col-6">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h3>Kirim Text dan Gambar</h3>
                            <div class="separator mb-5"></div>
                            <form method="post" action="<?php echo base_url('send/personal_image') ?>">
                                <div class="form-group">
                                    <input type="text" name="_key" value="<?php echo en($d->message_id) ?>" hidden readonly required>
                                    <select class="js-example-basic-multiple form-control" name="personal[]" multiple="multiple">
                                        <?php foreach ($receiver as $r) : ?>
                                            <option value="<?php echo $r->receiver_id ?>"><?php echo $r->receiver_name ?> ( <?php echo $r->id_number ?> )</option>
                                        <?php endforeach; ?>
                                    </select>
                                    <br><br>
                                    <div class="">
                                        <button type="submit" class="btn btn-success ld-ext-right" onclick="this.classList.toggle('running')"><i class="simple-icon-paper-plane"></i> Kirim <img src="<?php echo base_url() ?>assets/img/mini-loading.gif" class="ld"></button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card mb-4">
                        <div class="card-body">
                            <h3>Kirim Text Saja</h3>
                            <div class="separator mb-5"></div>
                            <form method="post" action="<?php echo base_url('send/text') ?>">

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <script>
        calculate = function() {
            var a = document.getElementById('a').value;
            var b = document.getElementById('b').value;
            document.getElementById('c').value = parseInt(a) + parseInt(b);

            var d = document.getElementById('d').value;
            var e = document.getElementById('e').value;
            document.getElementById('f').value = parseInt(d) + parseInt(e);
        }
    </script>
    <script>
        $(function() {
            $('.mySelect').on('change', function() {
                $.cookie('mySelect', this.value);
            });
            $('.mySelect').val($.cookie('mySelect') || 1);
        });
    </script>
<?php endforeach; ?>

<!-- Send Alert -->
<?php if ($this->session->flashdata('send_success')) : ?>
    <script type="text/javascript">
        swal({
            type: "success",
            title: "Terkirim",
            showConfirmButton: false,
            timer: 2500
        })
    </script>
<?php endif; ?>

<script>
    $(document).ready(function() {
        $('.js-example-basic-multiple').select2();
    });
</script>