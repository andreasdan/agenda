<?php foreach ($data as $m) : ?>

<div class="m-content">
    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30"
        role="alert">
        <div class="m-alert__icon">
            <i class="flaticon-questions-circular-button m--font-brand"></i>
        </div>
        <div class="m-alert__text">
            Klik <code>tab</code> paling atas untuk menyeleksi jenis PTK.
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Pilih <?php echo $title ?>
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <div>
                <ul class="nav nav-tabs justify-content-center mb-4" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link" href="<?php echo base_url('select/all/' . en($m->agenda_id)) ?>">
                            <p>Semua</p>
                        </a>
                    </li>
                    <?php
                    // loop department
                    foreach ($jenis_ptk as $jp) {
                    ?>
                    <li class="nav-item">
                        <a class="nav-link"
                            href="<?php echo base_url('select/jenisptk/' . en($m->agenda_id) . '/' . $jp->jenis_ptk_id) ?>">
                            <p><?php echo $jp->nama_jenis_ptk ?></p>
                        </a>
                    </li>
                    <?php } ?>
                    <li class="nav-item">
                        <button type="button" id="kirim" class="btn btn-outline-success">Kirim</button>
                    </li>
                </ul>
                <div>
                    <div class="col-sm-2" style="background-color: yellow;">
                        <input type="checkbox" id="check_all" value='1'> Pilih Semua
                    </div>
                    <br>
                    <div class="tab-pane fade show active justify-content-center" id="all" role="tabpanel">
                        <table id="datatable2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>No</th>
                                    <th>NIP</th>
                                    <th>Nama</th>
                                    <th>Telp</th>
                                    <th>Jenis PTK</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                foreach ($ptk as $d) :
                                ?>
                                <tr id='tr_<?php echo $d->ptk_id ?>'>
                                    <td align='center'><input type='checkbox' class='checkbox' name='kirim[]'
                                            value='<?php echo $d->ptk_id ?>'>
                                    </td>
                                    <td width="10%"><?php echo $no++ ?></td>
                                    <td><?php echo $d->nip ?></td>
                                    <td><?php echo $d->nama ?></td>
                                    <td><?php echo $d->telp ?></td>
                                    <td><?php echo $d->nama_jenis_ptk ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- end tab 1 -->
                </div>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>

<!-- Delete Checkbox -->
<script type="text/javascript">
$(document).ready(function() {
    var oTable = $('#datatable2').dataTable({
        pageLength: 5,
        lengthMenu: [5, 10, 25, 50, 100, 500, 1000],
        stateSave: true
    });

    var nodes = oTable.fnGetNodes();

    //$("#check_all").change(function() { // change = digunakan untuk memilih data dari 1 - akhir pada semua halaman
    $("#check_all").click(
        function() { // click = digunakan untuk memilih data dari 1 - akhir pada current halaman
            var checked = $(this).is(':checked');
            if (checked) {
                $('.checkbox').attr("checked", true);
            } else {
                $('.checkbox').attr("checked", false);
            }
        });

    // Saat klik pagination check_all true menjadi false
    $("#datatable2").on('page.dt', function() {
        $('.checkbox').attr("checked", false);
        $("#check_all").prop("checked", false);
    });

    // Changing state of check_all checkbox 
    $(".checkbox").click(function() {

        if ($(".checkbox").length == $(".checkbox:checked").length) {
            $("#check_all").prop("checked", true);
        } else {
            $("#check_all").prop("checked", false);
        }
    });

    $('#kirim').click(function() {
        var numberx = [];
        $(".checkbox:checked").each(function() {
            var phonex = $(this).val();
            numberx.push(phonex);
        });

        // Jika belum di checklist
        if (numberx == '') {
            Swal.fire({
                title: 'Kosong',
                text: 'Mohon pilih data penerima pesan!',
                type: "info",
            });
        } else {
            // Jika sudah dichecklist
            Swal.fire({
                title: 'Kirim',
                text: 'Konfirmasi kirim sekarang?',
                type: "question",
                confirmButtonColor: '#4ab562',
                showCancelButton: true,
            }).then((result) => {
                if (result.value) {
                    kirim(); // ke function kirim
                } else if (result.dismiss === swal.DismissReason.cancel) {
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 3000,
                        timerProgressBar: true,
                        onOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })
                    Toast.fire({
                        type: 'error',
                        title: 'Batal mengirim pesan'
                    })
                }
            })
        }
    });

    function kirim() {
        $(".checkbox:checked").each(function() {
            var number = $(this).val();

            var dataObj = {
                    ptk_id: number,
                    agenda_id: '<?php echo $m->agenda_id ?>',
                }

            $.ajax({
                url: '<?php echo base_url() ?>kirim/create',
                type: 'POST',
                dataType: 'json',
                data: dataObj,
                success: function(response) {
                    Swal.fire({
                        title: 'Mengirim',
                        text: 'Memulai pengiriman agenda, untuk melihat detail pengiriman pilih menu arsip',
                        type: "success",
                        confirmButtonColor: '#4ab562',
                    })
                }
            });
        });
    }

});
</script>
<?php endforeach; ?>