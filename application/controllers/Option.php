<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Option extends CI_Controller {

	public function __construct(){
		parent::__construct();

		if ($this->session->userdata('level') !== '1') {
			reject();
		}

		$this->load->model('dataoption');
		$this->load->helper('url');
	}

	public function index()
	{
		$data['data'] = $this->dataoption->get_all()->result();
		$data['title'] = "Pengaturan";
		$this->template->load('layout_dashboard', 'page_dashboard/option', $data);
	}

	public function update(){
		
		$encrypt_id = $this->input->post('_key');
		$id = de($encrypt_id);
		
		$data = array(
			'label' => $this->input->post('label')
		);
		
		$where = array(
			'option_id' => $id
		);
		
		$this->dataoption->update($where,$data,'options');
		$this->session->set_flashdata('update_success', 'Success!');
		redirect('option');
	}
	
}