<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
 
class Dataoption extends CI_Model{
	
	public function __construct()
        {
            $this->load->database();
        }

	public function get_all()
	{
		$this->db->from('options');
		return $this->db->get();
	}	
	
	public function edit($where,$table){		
		return $this->db->get_where($table,$where);
	}
	
	public function update($where,$data,$table){
		$this->db->where($where);
		$this->db->update($table,$data);
	}
	
}