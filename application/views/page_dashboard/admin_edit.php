<form id="form_edit" method="post">
    <div class="modal-body">
        <input type="hidden" id="id_" value="<?php echo en($e->admin_id) ?>" type="text" class="form-control input-default" required>
        <div class="form-group">
            <label>Nama</label>
            <input id="nama_" value="<?php echo $e->nama ?>" type="text" class="form-control input-default" maxlength="150" required autocomplete="off">
        </div>
        <div class="form-group">
            <label>Username</label>
            <input id="username_" value="<?php echo $e->username ?>" type="text" class="form-control input-default" maxlength="50" required autocomplete="off">
        </div>
        <div class="form-group">
            <label>Password</label>
            <input id="password_" type="password" class="form-control input-default" maxlength="25">
        </div>
    </div>
    <div class="modal-footer">
        <button type="reset" class="btn btn-outline-danger" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success">Simpan</button>
    </div>
</form>