<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Dataadmin extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function get_all()
    {
        $this->db->from("admin");
        $this->db->order_by("admin_id", "desc");
        return $this->db->get();
    }

    public function get_id($id)
    {
        $this->db->select('*');
        $this->db->from('admin');
        $this->db->where('admin_id', $id);
        return $this->db->get();
    }

    public function insert($record)
    {
        return $this->db->insert('admin', $record);
    }

    public function update($where, $data)
    {
        $this->db->where($where);
        $this->db->update('admin', $data);
    }

    # Delete Permanent
    function delete($where)
    {
        $this->db->where($where);
        $this->db->delete('admin');
    }
}
