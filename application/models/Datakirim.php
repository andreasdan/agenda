<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Datakirim extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}

	public function get_all()
	{
		$this->db->from("pengiriman k");
		$this->db->join("agenda a","k.agenda_id=a.agenda_id");
		$this->db->join("ptk p","k.ptk_id=p.ptk_id");
		$this->db->join("jenis_ptk j","p.jenis_ptk_id=j.jenis_ptk_id");
		$this->db->order_by("k.pengiriman_id", "asc");
		return $this->db->get();
	}

	public function get_id($x)
	{
		$this->db->where("pengiriman_id", $x);
		return $this->db->get("pengiriman");
	}

	public function get_basic_ptkid($ptk_id)
	{
		$this->db->from("pengiriman k");
		$this->db->join("agenda a","k.agenda_id=a.agenda_id");
		$this->db->join("ptk p","k.ptk_id=p.ptk_id");
		$this->db->join("jenis_ptk j","p.jenis_ptk_id=j.jenis_ptk_id");
		$this->db->where("k.ptk_id",$ptk_id);
		$this->db->order_by("k.pengiriman_id", "desc");
		return $this->db->get();
	}

	public function get_detail($pengiriman_id)
	{
		$this->db->from("pengiriman k");
		$this->db->join("agenda a","k.agenda_id=a.agenda_id");
		$this->db->join("ptk p","k.ptk_id=p.ptk_id");
		$this->db->join("jenis_ptk j","p.jenis_ptk_id=j.jenis_ptk_id");
		$this->db->where("k.pengiriman_id",$pengiriman_id);
		$this->db->order_by("k.pengiriman_id", "desc");
		return $this->db->get();
	}

	public function add($data)
	{
		return $this->db->insert('pengiriman', $data);
	}

	public function insertGetId($data)
	{
		$this->db->insert('pengiriman', $data);
		return $this->db->insert_id();;
	}

	function delete($where)
	{
		$this->db->where($where);
		$this->db->delete('pengiriman');
    }
}