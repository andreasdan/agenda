<?php foreach ($data as $d) : ?>
    <main>
        <div class="container-fluid" id="load-contents">
            <div class="row">
                <div class="col-12">
                    <h1>Detail <?php echo $title ?></h1>
                    <div class="separator mb-5"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-2">
                    <div class="card mb-4">
                        <div class=" card-body">
                            <div class="text-center">
                                <a href="<?php echo base_url() ?>uploads/<?php echo $d->image ?>" target="_blank">
                                    <img alt=" " src="<?php echo base_url() ?>uploads/<?php echo $d->image ?>" class="list-thumbnail responsive border-0">
                                </a>
                                <br><br>
                                <button type="button" class="btn btn-outline-primary" data-toggle="modal" data-backdrop="static" data-target="#modalImage">Ubah Foto</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-10">
                    <div class="card mb-4">
                        <div class="card-body">
                            <form method="post" action="<?php echo base_url('message/update') ?>" name="form" autocomplete="off" enctype="multipart/form-data">
                                <div id="dynamicContent">
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label>Preview:</label>
                                            <div id="preview" class="unselectable">
                                                <font color="silver">Text Simulation</font>
                                            </div>
                                            </td>
                                        </div>
                                        <br><br><br>
                                        <div class="form-group">
                                            <div class="btn-group btn-group-sm  mr-2  mb-1" role="group">
                                                <button type="button" onclick="javascript: format(0); this.blur();" name="buttonBold" id="buttonBold" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Bold"><b>B</b></button>
                                                <button type="button" onclick="javascript: format(1); this.blur();" disabled="disabled" name="buttonItalic" id="buttonItalic" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Italic"><i>I</i></button>
                                                <button type="button" onclick="javascript: format(2); this.blur();" disabled="disabled" name="buttonStrikethrough" id="buttonStrikethrough" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Strikethrough"><s>S</s></button>
                                                <button type="button" onclick="javascript: format(3); this.blur();" disabled="disabled" name="buttonMonospace" id="buttonMonospace" class="btn btn-secondary" data-toggle="tooltip" data-placement="top" title="Monospace"><tt>M</tt></button>
                                            </div>
                                        </div>
                                        <input type="text" name="_key" value="<?php echo en($d->message_id) ?>" hidden required>
                                        <div class="form-group">
                                            <label>Isi Pesan</label>
                                            <textarea name="whatsappText" id="whatsappText" class="form-control" onkeyup="javascript: update();" onmouseup="javascript: update();" onselect="javascript: whatsappTextSelected();" placeholder="WhatsApp Message Text"><?php echo $d->content ?></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Keterangan</label>
                                            <input type="text" name="memo" value="<?php echo $d->memo ?>" class="form-control">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- Modal Image -->
    <div class="modal fade bd-example-modal-lg" id="modalImage" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <form action="<?php echo base_url('message/update_image') ?>" method="post" enctype="multipart/form-data">
                    <div class="modal-header">
                        <h5 class="modal-title">Ubah <?php echo $title ?></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="text" name="_key" value="<?php echo en($d->message_id) ?>" hidden required autofocus>
                        <input type="text" name="old_image" value="<?php echo $d->image ?>" hidden>
                        <div class="form-group">
                            <input type="file" name="file_image">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-outline-danger" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php endforeach; ?>

<!-- Update Alert -->
<?php if ($this->session->flashdata('update_success')) : ?>
    <script type="text/javascript">
        swal({
            type: "success",
            title: "Data telah diubah",
            showConfirmButton: false,
            timer: 2500
        })
    </script>
<?php endif; ?>

<?php if ($this->session->flashdata('image_error')) : ?>
    <script type="text/javascript">
        swal({
            type: "error",
            title: "Foto tidak valid",
            text: "Coba ganti dengan file lainnya",
            confirmButtonColor: '#d9534f',
            timer: 30000
        })
    </script>
<?php endif; ?>