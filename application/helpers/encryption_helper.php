<?php
function en($string, $key = "", $url_safe = TRUE)
{
    if ($key == null || $key == "") {
        $key = "andreas_sdflewr438543iplfm80oo3435l_ardani";
    }
    $CI = &get_instance();
    $ret = $CI->encrypt->encode($string, $key);

    if ($url_safe) {
        $ret = strtr(
            $ret,
            array(
                '+' => '______',
                '=' => '____',
                '/' => '__'
            )
        );
    }

    return $ret;
}
function de($string, $key = "")
{
    if ($key == null || $key == "") {
        $key = "andreas_sdflewr438543iplfm80oo3435l_ardani";
    }
    $CI = &get_instance();
    $string = strtr(
        $string,
        array(
            '______' => '+',
            '____' => '=',
            '__' => '/'
        )
    );

    return $CI->encrypt->decode($string, $key);
}
