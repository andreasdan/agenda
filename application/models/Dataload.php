<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Dataload extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
        $this->load->helper();
    }

    public function user_session($id)
    {
        $sql = "SELECT * FROM admin WHERE admin_id = ? LIMIT 1";
        $parameter = $this->db->query($sql, array($id));
        return $parameter->result();
    }
    
    public function option($x)
	{
		return $this->db->query("SELECT * FROM options WHERE name='$x'")->row();
	}
}
