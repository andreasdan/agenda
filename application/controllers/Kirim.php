<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kirim extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		if ($this->session->userdata('login_admin') == FALSE) {
			reject();
		}

		$this->load->model('datakirim');
		$this->load->model('dataptk');
		$this->load->model('dataagenda');
		$this->load->helper('url');
		$this->load->library('encrypt');
	}

	public function index()
	{
		$data['title'] = "Detail Pengiriman Agenda"; //title
		$data['url'] = base_url('kirim/all');

		$this->template->load('layout_dashboard', 'page_dashboard/kirim', $data);
	}

	public function all()
    {
        $record = $this->datakirim->get_all()->result();
        $no = 1;
        foreach ($record as $d) {
            $tbody = array();
            $tbody[] = $no++;
            $tbody[] = tanggal($d->tanggal_kirim);
            $tbody[] = $d->nama;
            $tbody[] = $d->nama_jenis_ptk;
            $tbody[] = $d->nama_agenda;
            $tbody[] = tanggal($d->tanggal_agenda);
            $tbody[] = jam($d->tanggal_agenda).' WIB';
            $tbody[] = $d->tempat;
            $tbody[] = $d->keterangan;
            $tbody[] = '<button class="delete-data btn btn-danger" data-id=' . en($d->pengiriman_id) . '>Hapus</button>';
            $data[] = $tbody;
        }

        if ($record) {
            echo json_encode(array('data' => $data));
        } else {
            echo json_encode(array('data' => 0));
        }
	}

	public function create()
	{
        $ptk_id = $this->input->post('ptk_id');
        $agenda_id = $this->input->post('agenda_id');

        $user = $this->dataptk->get_id($ptk_id)->row();
        $agenda = $this->dataagenda->get_id($agenda_id)->row();

        $record = array(
         'agenda_id' => $agenda_id,
         'ptk_id' => $ptk_id,
        );
        $pengirimanId = $this->datakirim->insertGetId($record);

        $authKey = "AAAAKAyOYJg:APA91bEHiqGB_paTVhk0mqrk20KgI7mvhx9usgkVo9Lxv8xjpJrNSAkQ0rPrMJUdzh69PKoGpxzHBoI_U_y17S5QQ8GK_McgpYuCxLJSDDbhAE27m_k7mWuu-TBslMo7yRCwhNbYA-P4";
        $device_token = $user->device_token;
        $body = [
            "to" => $device_token,
            "notification" => [
                "title" => $agenda->nama_agenda,
                "body" => tanggal($agenda->tanggal_agenda),
            ],
            "data" => [
                "agendaId" => $pengirimanId
            ]
        ];

        /* FCM */
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($body),
        CURLOPT_HTTPHEADER => array(
            "Authorization: key=" . $authKey,
            "Content-Type: application/json",
            "cache-control: no-cache"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }
    }

	public function delete()
    {
        $id = de($this->input->post('id'));
        $where = array(
            'pengiriman_id' => $id
        );
        $data = $this->datakirim->delete($where);
        echo json_encode($data);
    }
}