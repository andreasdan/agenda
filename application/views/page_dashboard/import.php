<script>
$(document).ready(function() {
    // Sembunyikan alert validasi kosong
    $("#kosong").hide();
});
</script>

<!-- <main>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <h1>Import Data</h1>
                <div class="separator mb-5"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card mb-4">
                    
                </div>
            </div>
        </div>
    </div>
</main> -->

<div class="m-content">
    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30"
        role="alert">
        <div class="m-alert__icon">
            <i class="flaticon-questions-circular-button m--font-brand"></i>
        </div>
        <div class="m-alert__text">
            Telusuri file Microsoft Excel berformat <code>.xlxs</code> kemudian pilih <code>tombol pratinjau</code>.
            Pastikan baris dan kolom data tidak ada yang kosong.
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        <?php echo $title ?>
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <div>
                <div class="card-body">
                    <form method="post" action="<?php echo base_url("import/form"); ?>" enctype="multipart/form-data">
                        <input type="file" name="file" required>
                        <input class="btn btn-outline-primary" type="submit" name="preview" value="Pratinjau">
                    </form>
                    <br>
                </div>
                <div class="card-body">
                    <?php
                        if (isset($_POST['preview'])) { // Jika user menekan tombol Preview pada form
                            if (isset($upload_error)) { // Jika proses upload gagal
                                echo "<div style='color: red;'>" . $upload_error . "</div>"; // Muncul pesan error upload
                                die; // stop skrip
                            }

                            // Buat sebuah tag form untuk proses import data ke database
                            echo "<form method='post' action='" . base_url("import/excel") . "'>";

                            // Buat sebuah div untuk alert validasi kosong
                            echo "<div style='color: red;' id='kosong'>
                                    Semua data belum diisi, Ada <span id='jumlah_kosong'></span> data yang belum diisi.
                                    </div>";

                            echo "<table class='table table-bordered table-hover'>
                                    <tr>
                                        <th>NIK</th>
                                        <th>Nama</th>
                                        <th>Nomor Telp</th>
                                        <th>Grade</th>
                                    </tr>";

                            $numrow = 1;
                            $kosong = 0;

                            // Lakukan perulangan dari data yang ada di excel
                            // $sheet adalah variabel yang dikirim dari controller
                            foreach ($sheet as $row) {
                                // Ambil data pada excel sesuai Kolom
                                $id_number = $row['A']; // Ambil data NIS
                                $receiver_name = $row['B']; // Ambil data nama
                                $phone_number = $row['C']; // Ambil data jenis kelamin
                                $grade  = $row['D']; // Ambil data alamat

                                // Cek jika semua data tidak diisi
                                if ($id_number == "" && $receiver_name == "" && $phone_number == "" && $grade  == "")
                                    continue; // Lewat data pada baris ini (masuk ke looping selanjutnya / baris selanjutnya)

                                // Cek $numrow apakah lebih dari 1
                                // Artinya karena baris pertama adalah nama-nama kolom
                                // Jadi dilewat saja, tidak usah diimport
                                if ($numrow > 1) {
                                    // Validasi apakah semua data telah diisi
                                    $id_number_td = (!empty($id_number)) ? "" : " style='background: #E07171;'"; // Jika NIS kosong, beri warna merah
                                    $receiver_name_td = (!empty($receiver_name)) ? "" : " style='background: #E07171;'"; // Jika Nama kosong, beri warna merah
                                    $jk_td = (!empty($phone_number)) ? "" : " style='background: #E07171;'"; // Jika Jenis Kelamin kosong, beri warna merah
                                    $grade_td = (!empty($grade)) ? "" : " style='background: #E07171;'"; // Jika Alamat kosong, beri warna merah

                                    // Jika salah satu data ada yang kosong
                                    if ($id_number == "" or $receiver_name == "" or $phone_number == "" or $grade  == "") {
                                        $kosong++; // Tambah 1 variabel $kosong
                                    }

                                    echo "<tr>";
                                    echo "<td" . $id_number_td . ">" . $id_number . "</td>";
                                    echo "<td" . $receiver_name_td . ">" . $receiver_name . "</td>";
                                    echo "<td" . $jk_td . ">" . $phone_number . "</td>";
                                    echo "<td" . $grade_td . ">" . $grade  . "</td>";
                                    echo "</tr>";
                                }

                                $numrow++; // Tambah 1 setiap kali looping
                            }

                            echo "</table>";

                            // Cek apakah variabel kosong lebih dari 0
                            // Jika lebih dari 0, berarti ada data yang masih kosong
                            if ($kosong > 0) {
                                ?>
                    <script>
                    $(document).ready(function() {
                        // Ubah isi dari tag span dengan id jumlah_kosong dengan isi dari variabel kosong
                        $("#jumlah_kosong").html('<?php echo $kosong; ?>');

                        $("#kosong").show(); // Munculkan alert validasi kosong
                    });
                    </script>
                    <?php
                                } else { // Jika semua data sudah diisi
                                    echo "<hr>";
                                    ?>

                    <div class="form-group col-md-3">
                        <select class="form-control" name="department" required>
                            <option value="">- Pilih Unit/Departement -</option>
                            <option value="1"><?php echo department(1) ?></option>
                            <option value="2"><?php echo department(2) ?></option>
                            <option value="3"><?php echo department(3) ?></option>
                            <option value="4"><?php echo department(4) ?></option>
                            <option value="5"><?php echo department(5) ?></option>
                            <option value="6"><?php echo department(6) ?></option>
                            <option value="7"><?php echo department(7) ?></option>
                            <option value="8"><?php echo department(8) ?></option>
                            <option value="9"><?php echo department(9) ?></option>
                        </select>
                    </div>

                    <?php

                            // Buat sebuah tombol untuk mengimport data ke database
                            echo "<div class='modal-footer'>";
                            echo "<a href='" . base_url("receiver") . "' type='submit' class='btn btn-outline-danger'>Kembali</a>";
                            echo "<button type='submit' class='btn btn-primary'>Import</button>";
                            echo "</div>";
                            }
                            echo "</form>";
                        }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>