<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Dataagenda extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}

	public function get_all()
	{
		$this->db->from("agenda");
		$this->db->order_by("agenda_id", "asc");
		return $this->db->get();
	}

	public function get_id($x)
	{
		$this->db->where("agenda_id", $x);
		return $this->db->get("agenda");
	}

	public function add($data)
	{
		return $this->db->insert('agenda', $data);
	}

	public function edit($x, $y)
	{
		$this->db->where($x, $y);
		return $this->db->get("agenda");
	}

	public function update($where, $data)
	{
		$this->db->where($where);
		$this->db->update('agenda', $data);
	}

	function delete($where)
	{
		$this->db->where($where);
		$this->db->delete('agenda');
    }
}