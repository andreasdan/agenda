<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Datamessage extends CI_Model
{

    public function get_all()
    {
        $this->db->from("messages");
        $this->db->order_by("message_id", "desc");
        return $this->db->get();
    }

    public function get_id($id)
    {
        $this->db->select('*');
        $this->db->from('messages');
        $this->db->where('message_id', $id);
        //$this->db->limit(100);
        return $this->db->get();
    }

    public function insert($record)
    {
        return $this->db->insert('messages', $record);
    }

    public function update($where, $data)
    {
        $this->db->where($where);
        $this->db->update('messages', $data);
    }

    # Delete Permanent
    function delete($where)
    {
        $this->db->where($where);
        $this->db->delete('messages');
    }
}
