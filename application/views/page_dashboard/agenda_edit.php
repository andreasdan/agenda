<form id="form_edit" name="form2" method="POST" enctype="multipart/form-data">
    <div class="modal-body">
        <input type="hidden" name="id" value="<?php echo en($e->agenda_id) ?>" type="text"
            class="form-control input-default" required>
        <div class="form-group">
            <label>Nama Agenda</label>
            <input type="text" name="nama" value="<?php echo $e->nama_agenda ?>" class="form-control">
        </div>
        <div class="form-group">
            <label>Tanggal dan Jam</label>
            <input type="text" name="tanggal" value="<?php echo $e->tanggal_agenda ?>" class="datetimepicker form-control">
        </div>
        <div class="form-group">
            <label>Tempat</label>
            <input type="text" name="tempat" value="<?php echo $e->tempat ?>" class="form-control">
        </div>
        <div class="form-group">
            <label>Keterangan</label>
            <input type="text" name="keterangan" value="<?php echo $e->keterangan ?>" class="form-control">
        </div>
    </div>
    <div class="modal-footer">
        <button type="reset" class="btn btn-outline-danger" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success">Simpan</button>
    </div>
</form>

<script src="<?php echo base_url() ?>assets/demo/default/custom/crud/forms/widgets/bootstrap-datetimepicker.js" type="text/javascript"></script>
<script>
    $(function () {
        $('.datetimepicker').datetimepicker();
    });
</script>