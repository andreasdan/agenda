<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();

		$this->load->model('datakirim');
        $this->load->model('dataptk');
		$this->load->model('dataagenda');
		$this->load->helper('url');
		$this->load->library('encrypt');
	}

    // Profil
	public function profil($ptk_id)
    {
        $x = $this->dataptk->get_ptk($ptk_id)->row();
        echo json_encode([
            'nama_ptk' => $x->nama,
            'nama_jenis_ptk' => $x->nama_jenis_ptk,
        ]);
	}

    // Pengiriman Agenda ke mobile ptk/user
	public function pengiriman($ptk_id)
    {
        $query = $this->datakirim->get_basic_ptkid($ptk_id)->result();
        $record = [];
        foreach($query as $i => $d){
            $record[$i] = [];
            $record[$i]['id'] = (int)$d->pengiriman_id;
            $record[$i]['nama_agenda'] = $d->nama_agenda;
            $record[$i]['tanggal_agenda'] = tanggal($d->tanggal_agenda);
        }
        echo json_encode([
            'code' => 200,
            'data' => $record,
        ]);
	}

    // Detail Pengiriman Agenda ke mobile ptk/user
	public function detail_pengiriman($pengiriman_id)
    {
        $query = $this->datakirim->get_detail($pengiriman_id)->row();
        if (!empty($query)) {
            $record['id'] = (int)$query->pengiriman_id;
            $record['nama_agenda'] = $query->nama_agenda;
            $record['tanggal_agenda'] = tanggal($query->tanggal_agenda); // Tanggal Agenda
            $record['jam'] = jam($query->tanggal_agenda).' WIB';
            $record['tempat'] = $query->tempat;
            $record['keterangan'] = $query->keterangan;
            $record['tanggal_kirim'] = tanggal($query->tanggal_kirim); // Tanggal Pengiriman

            echo json_encode([
                'code' => 200,
                'data' => $record,
            ]);
        }else{
            echo json_encode([
                'code' => 404,
                'data' => null,
            ]);
        }
	}

    // Reset Password
	public function reset()
	{
        $record = array(
			'password' => md5($this->input->post('password')),
		);
        $where = array(
            'ptk_id' => $this->input->post('ptk_id'),
        );
		if($this->dataptk->update($where, $record)){
    		echo json_encode([
                'code' => 200,
                'message' => "Sukses reset password",
            ]);

        }else{
            echo json_encode([
                'code' => 400,
                'message' => "Gagal reset password",
            ]);
        }
    }

    // Login PTK
	function login()
    {
        $nip = $this->input->post('nip');
        $password = $this->input->post('password');

        $cek_user = $this->dataptk->auth_ptk_nip($nip, $password);

        if ($cek_user->num_rows() == 1) {
            $ptkId = $cek_user->row()->ptk_id;
            $email = $cek_user->row()->email;

            $record = array(
                'device_token' => $this->input->post('device_token'),
            );
            $where = array(
                'email' => $email,
            );
            $this->dataptk->update($where, $record);

            echo json_encode([
                'code' => 200,
                'message' => "Login Berhasil",
                'ptk_id' => (int)$ptkId,
            ]);
        } else {
            echo json_encode([
                'code' => 400,
                'message' => "Pengguna Tidak Ditemukan",
            ]);
        }
    }

    public function logout()
    {
        $ptkId = $this->input->post('ptk_id');
        $cekPtk = $this->dataptk->get_ptk($ptkId);

        if($cekPtk->row() != null){
            $where = array(
                'ptk_id' => $ptkId,
            );
            $this->dataptk->update($where, array('device_token' => ""));

            echo json_encode([
                'code' => 200,
                'message' => 'Sukses Logout'
            ]);
        }else{
            echo json_encode([
                'code' => 400,
                'message' => 'Ada kesalahan'
            ]);
        }
        
    }

    // untuk testing FCM di API
    public function create()
    {   
        $ptk_id = $this->input->post('ptk_id');
        $agenda_id = $this->input->post('agenda_id');

        $user = $this->dataptk->get_id($ptk_id)->row();
        $agenda = $this->dataagenda->get_id($agenda_id)->row();

        $record = array(
         'agenda_id' => $agenda_id,
         'ptk_id' => $ptk_id,
        );
        $pengirimanId = $this->datakirim->insertGetId($record);

        $authKey = "AAAAKAyOYJg:APA91bEHiqGB_paTVhk0mqrk20KgI7mvhx9usgkVo9Lxv8xjpJrNSAkQ0rPrMJUdzh69PKoGpxzHBoI_U_y17S5QQ8GK_McgpYuCxLJSDDbhAE27m_k7mWuu-TBslMo7yRCwhNbYA-P4";
        $device_token = $user->device_token;
        $body = [
            "to" => $device_token,
            "notification" => [
                "title" => $agenda->nama_agenda,
                "body" => tanggal($agenda->tanggal_agenda),
            ],
            "data" => [
                "agendaId" => $pengirimanId
            ]
        ];

        /* FCM */
        $curl = curl_init();
        curl_setopt_array($curl, array(
        CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => json_encode($body),
        CURLOPT_HTTPHEADER => array(
            "Authorization: key=" . $authKey,
            "Content-Type: application/json",
            "cache-control: no-cache"
        ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            echo $response;
        }
    }
}