<?php
class Auth extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('dataauth');
    }

    function index()
    {
        $data['title'] = "Login Admin";
        $this->load->view('layout_auth', $data);
    }

    function login()
    {
        $username = htmlspecialchars($this->input->post('username', TRUE), ENT_QUOTES);
        $password = htmlspecialchars($this->input->post('password', TRUE), ENT_QUOTES);

        $cek_user = $this->dataauth->auth_user($username, $password);

        if ($cek_user->num_rows() == 1) { //jika login sebagai Petugas
            foreach ($cek_user->result() as $data) {
                $user_data = array(
                    'id' => $data->admin_id,
                    'login_admin' => TRUE
                );
                $this->session->set_userdata($user_data);
            }
            $this->session->set_flashdata('login_success', 'Success!');
            redirect('dashboard');
        } else {
            $this->session->set_flashdata('login_failed', 'Error!');
            redirect('auth');
        }
    }

    function logout()
    {
        $this->session->sess_destroy();
        redirect('auth');
    }
}
