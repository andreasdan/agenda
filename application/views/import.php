<!-- END: Subheader -->
<div class="m-content">
    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30"
        role="alert">
        <div class="m-alert__icon">
            <i class="flaticon-questions-circular-button m--font-brand"></i>
        </div>
        <div class="m-alert__text">
            Halaman ini untuk import data <code>Excel</code> ke <code>Database</code>.
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        <?php echo $title ?>
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <div style="font-size:16px;">
                <div>
                    <form method="post" id="import_form" enctype="multipart/form-data">
                        <p><label>Select Excel File</label>
                            <input type="file" name="file" id="file" required accept=".xls, .xlsx" />
                        </p>
                        <br />

                        <div class="form-group">
                            <label>Level</label>
                            <select id="dapartement" class="form-control" name="dapartement">
                                <option value="">Pilih</option>
                                <option value="1"><?php echo department(1) ?></option>
                                <option value="2"><?php echo department(2) ?></option>
                                <option value="3"><?php echo department(3) ?></option>
                                <option value="4"><?php echo department(4) ?></option>
                                <option value="5"><?php echo department(5) ?></option>
                                <option value="6"><?php echo department(6) ?></option>
                                <option value="7"><?php echo department(7) ?></option>
                                <option value="8"><?php echo department(8) ?></option>
                                <option value="9"><?php echo department(9) ?></option>
                            </select>
                        </div>

                        <input type="submit" name="import" value="Import" class="btn btn-info" />

                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>

<script>
$(document).ready(function() {

    load_data();

    function load_data() {
        $.ajax({
            url: "<?php echo base_url(); ?>import/fetch",
            method: "POST",
            success: function(data) {
                $('#customer_data').html(data);
            }
        })
    }

    $('#import_form').on('submit', function(event) {
        event.preventDefault();
        $.ajax({
            url: "<?php echo base_url(); ?>import/import",
            method: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function(data) {
                $('#file').val('');
                //$('#dapartement').val('');
                load_data();
                alert(data);
            }
        })
    });

});
</script>