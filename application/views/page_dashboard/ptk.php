<div class="m-content">
    <div class="m-alert m-alert--icon m-alert--air m-alert--square alert alert-dismissible m--margin-bottom-30"
        role="alert">
        <div class="m-alert__icon">
            <i class="flaticon-questions-circular-button m--font-brand"></i>
        </div>
        <div class="m-alert__text">
            Halaman ini digunakan untuk mengolah halaman <?php echo $title ?> meliputi membuat <?php echo $title ?>
            baru, edit <?php echo $title ?> dan menonaktifkan <?php echo $title ?>.
        </div>
    </div>
    <div class="m-portlet m-portlet--mobile">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        Daftar <?php echo $title ?>
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                <ul class="m-portlet__nav">
                    <li class="m-portlet__nav-item">
                        <button type="button"
                            class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air"
                            data-toggle="modal" data-target="#modal_form_input">
                            <span>
                                <i class="la la-plus"></i>
                                <span><?php echo $title ?></span>
                            </span>
                        </button>
                    </li>
                </ul>
            </div>
        </div>
        <div class="m-portlet__body">
            <div class="table-responsive">

                <!--begin: Datatable -->
                <table class="data_table table table-striped- table-bordered table-hover table-checkable" id="">
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>NIP</th>
                            <th>Nama</th>
                            <th>Telp</th>
                            <th>Jenis PTK</th>
                            <th>Email</th>
                            <th width="10%">Pilihan</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>

<!-- Modal Create -->
<div class="modal fade bd-example-modal-lg" id="modal_form_input" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <form name="form" id="form_input" method="POST" enctype="multipart/form-data">
                <div class="modal-header">
                    <h5 class="modal-title">Input <?php echo $title ?></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="dynamicContent">
                        <div class="modal-body">
                            <div class="form-group">
                                <label>NIP</label>
                                <input type="text" name="nip" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Nama</label>
                                <input type="text" name="nama" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Telp</label>
                                <input type="text" name="telp" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Jenis PTK*</label>
                                <select name="jenis_ptk_id" class="select2 form-control input-default" style="width:100%" required>
                                    <option value="">Pilih Jenis PTK</option>
                                    <?php foreach ($jenis_ptk as $j) { ?>
                                    <option value="<?php echo $j->jenis_ptk_id ?>"><?php echo $j->nama_jenis_ptk ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <input type="text" name="email" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label>Password</label>
                                <input type="text" name="password" class="form-control" required>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-danger" data-dismiss="modal">Batal</button>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Edit -->
<div class="modal fade bd-example-modal-lg" id="modal_form_edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Edit <?php echo $title ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="page_edit">
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    // ini adalah fungsi untuk mengambil data ke dalam datatable
    var mydata = $('.data_table').DataTable({
        "processing": true,
        "ajax": "<?php echo $url ?>",
        stateSave: true,
        "searching": true,
        "order": []
    })
    // fungsi untuk menambah data
    $('#form_input').on('submit', function() {
        $.ajax({
            url: "<?php echo base_url('ptk/create') ?>",
            type: "POST",
            data: new FormData($('#form_input')[0]),
            processData: false,
            contentType: false,
            success: function(data) {
                mydata.ajax.reload(null, false);
                // Toast sukses
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })
                Toast.fire({
                    type: 'success',
                    title: 'Menambahkan <?php echo $title ?>'
                })
                // bersihkan form pada modal
                $('#modal_form_input').modal('hide');
                $("#form_input")[0].reset();

            }
        })
        return false;
    });
    // fungsi untuk edit data
    //pilih selector dari table id datamahasiswa dengan class .ubah-mahasiswa
    $('.data_table').on('click', '.edit-data', function() {
        $.ajax({
            type: "post",
            url: "<?php echo base_url('ptk/edit') ?>",
            data: {
                id: $(this).data('id')
            },
            success: function(data) {
                swal.close();
                $('#modal_form_edit').modal('show');
                $('#page_edit').html(data);

                // proses untuk mengubah data
                $('#form_edit').on('submit', function() {

                    $.ajax({
                        url: "<?php echo base_url('ptk/update') ?>",
                        type: "POST",
                        data: new FormData($('#form_edit')[0]),
                        processData: false,
                        contentType: false,
                        success: function(data) {
                            mydata.ajax.reload(null, false);
                            // Toast sukses
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                onOpen: (toast) => {
                                    toast.addEventListener(
                                        'mouseenter',
                                        Swal.stopTimer)
                                    toast.addEventListener(
                                        'mouseleave',
                                        Swal.resumeTimer
                                    )
                                }
                            })
                            Toast.fire({
                                type: 'success',
                                title: 'Menyunting <?php echo $title ?>'
                            })
                            // bersihkan form pada modal
                            $('#modal_form_edit').modal('hide');
                        }
                    })
                    return false;
                });
            }
        });
    });
    // fungsi untuk hapus data
    //pilih selector dari table id dengan class .delete-data
    $('.data_table').on('click', '.delete-data', function() {
        Swal.fire({
            title: 'Konfirmasi',
            text: "Anda yakin menghapus data <?php echo $title ?>?",
            type: 'question',
            showCancelButton: true,
            confirmButtonText: 'Ya, Hapus',
            confirmButtonColor: '#ff5e5e',
            cancelButtonColor: '#3085d6',
            cancelButtonText: 'Batal',
            reverseButtons: true
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    url: "<?php echo base_url('ptk/delete') ?>",
                    method: "post",
                    data: {
                        id: $(this).data('id')
                    },
                    success: function(data) {
                        // Toast sukses
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000,
                            timerProgressBar: true,
                            onOpen: (toast) => {
                                toast.addEventListener('mouseenter',
                                    Swal.stopTimer)
                                toast.addEventListener('mouseleave',
                                    Swal.resumeTimer)
                            }
                        })
                        Toast.fire({
                            type: 'success',
                            title: 'Menghapus <?php echo $title ?>'
                        })
                        mydata.ajax.reload(null, false)
                    }
                })
            } else if (result.dismiss === swal.DismissReason.cancel) {
                // Toast sukses
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 3000,
                    timerProgressBar: true,
                    onOpen: (toast) => {
                        toast.addEventListener('mouseenter', Swal.stopTimer)
                        toast.addEventListener('mouseleave', Swal.resumeTimer)
                    }
                })
                Toast.fire({
                    type: 'error',
                    title: 'Batal menghapus <?php echo $title ?>'
                })
            }
        })
    });

});
</script>