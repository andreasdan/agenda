<form id="form_edit" name="form2" method="POST" enctype="multipart/form-data">
    <div class="modal-body">
        <input type="hidden" name="id" value="<?php echo en($e->ptk_id) ?>" type="text"
            class="form-control input-default" required>
            <div class="form-group">
                <label>NIP</label>
                <input type="text" name="nip" value="<?php echo $e->nip ?>" class="form-control">
            </div>
            <div class="form-group">
                <label>Nama</label>
                <input type="text" name="nama" value="<?php echo $e->nama ?>" class="form-control">
            </div>
            <div class="form-group">
                <label>Telp</label>
                <input type="text" name="telp" value="<?php echo $e->telp ?>" class="form-control">
            </div>
            <div class="form-group">
                <label>Jenis PTK*</label>
                <select name="jenis_ptk_id" class="select2 form-control input-default" style="width:100%">
                    <option value="">Pilih Jenis PTK</option>
                    <?php foreach ($jenis_ptk as $j) { ?>
                    <option value="<?php echo $j->jenis_ptk_id ?>" <?php echo $j->jenis_ptk_id == $e->ptk_id ? 'selected' : ''; ?>><?php echo $j->nama_jenis_ptk ?></option>
                    <?php } ?>
                </select>
            </div>
            <div class="form-group">
                <label>Email</label>
                <input type="text" name="email" value="<?php echo $e->email ?>" class="form-control">
            </div>
            <div class="form-group">
                <label>Password</label>
                <input type="text" name="password" class="form-control">
            </div>
    </div>
    <div class="modal-footer">
        <button type="reset" class="btn btn-outline-danger" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success">Simpan</button>
    </div>
</form>