<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();

        if ($this->session->userdata('login_admin') == FALSE) {
            reject();
        }

        $this->load->helper('url');
        $this->load->library('encryption');
    }

    public function index()
    {
        redirect('admin');
    }
}
