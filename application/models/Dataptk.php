<?php
defined('BASEPATH') or exit('No direct script access allowed');


class Dataptk extends CI_Model
{

	public function __construct()
	{
		$this->load->database();
	}

	function auth_ptk($email, $password)
	{
		$sql = "SELECT * FROM ptk WHERE email = ? AND password = md5(?) LIMIT 1";
		$parameter = $this->db->query($sql, array($email, $password));
		return $parameter;
	}

	public function auth_ptk_nip($nip, $password)
	{
		return $this->db->get_where('ptk', array('nip' => $nip, 'password' => md5($password)));
	}

	public function get_all()
	{
		$this->db->from("ptk p");
		$this->db->join("jenis_ptk j","j.jenis_ptk_id=p.jenis_ptk_id");
		$this->db->order_by("p.ptk_id", "asc");
		return $this->db->get();
	}

	public function get_id($x)
	{
		$this->db->where("ptk_id", $x);
		return $this->db->get("ptk");
	}

	public function get_ptk($ptk_id)
	{
		$this->db->from("ptk p");
		$this->db->join("jenis_ptk j","j.jenis_ptk_id=p.jenis_ptk_id");
		$this->db->where("p.ptk_id", $ptk_id);
		return $this->db->get();
	}

	public function get_jenis_ptk($x)
	{
		$this->db->from("ptk p");
		$this->db->join("jenis_ptk j","j.jenis_ptk_id=p.jenis_ptk_id");
		$this->db->where("p.jenis_ptk_id", $x);
		$this->db->order_by("p.ptk_id", "asc");
		return $this->db->get();
	}

	public function get_phone($x)
	{
		$this->db->where("phone_number", $x);
		return $this->db->get("ptk");
	}

	public function get_email($x)
	{
		$this->db->where("email", $x);
		return $this->db->get("ptk");
	}

	public function add($data)
	{
		return $this->db->insert('ptk', $data);
	}

	public function edit($x, $y)
	{
		$this->db->where($x, $y);
		return $this->db->get("ptk");
	}

	public function update($where, $data)
	{
		$this->db->where($where);
		return $this->db->update('ptk', $data);
	}

	function delete($where)
	{
		$this->db->where($where);
		return $this->db->delete('ptk');
   }
}